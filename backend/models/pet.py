from services import db

class Pet(db.Model):
    __tablename__ = "pet"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    organization_id = db.Column(db.Text)
    type = db.Column(db.Text)
    breeds = db.Column(db.Text)
    age = db.Column(db.Text)
    gender = db.Column(db.Text)
    age = db.Column(db.Text)
    size = db.Column(db.Text)
    status = db.Column(db.Text)
    email = db.Column(db.Text) # in the contact field
    phone = db.Column(db.Text) # in the contact field
    address = db.Column(db.Text) # in the contact field
    photo_url = db.Column(db.Text) # photos.large field
    lat = db.Column(db.Float)
    lang = db.Column(db.Float)
    closest_vet = db.Column(db.Text)
  

    def serialize(self):
        serialObj = dict({
            'id' : self.id,
            'name': self.name,
            'organization_id': self.organization_id,
            'type': self.type,
            'breeds': self.breeds,
            'age': self.age,
            'gender': self.gender,
            'size': self.size,
            'status': self.status,
            'email': self.email,
            'phone':self.phone,
            'address':self.address,
            'photo_url':self.photo_url,
            'lat' : self.lat,
            'lang' : self.lang,
            'closest_vet' : self.closest_vet,
        })
        return serialObj
