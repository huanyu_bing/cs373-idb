from services import db

class Organization(db.Model):
    __tablename__ = "Organization"
    id = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String())
    email = db.Column(db.String())
    phone = db.Column(db.String())
    hours = db.Column(db.String())
    website = db.Column(db.Text)
    photos = db.Column(db.Text)
    facebook_url = db.Column(db.Text)
    twitter_url = db.Column(db.Text)
    instagram_url = db.Column(db.Text)
    formatted_address = db.Column(db.Text)
    rating = db.Column(db.Integer)
    open_now = db.Column(db.Text)
    business_status = db.Column(db.Text)
    rating_count = db.Column(db.Integer)
    lat = db.Column(db.Float)
    lang = db.Column(db.Float)
    closest_vet = db.Column(db.Text)
    pets = db.Column(db.Text)

    def serialize(self):
        serialObj = dict({
            'id' : self.id,
            'name': self.name,
            'email': self.email,
            'phone': self.phone,
            'hours': self.hours,
            'website': self.website,
            'photos': self.photos,
            'facebook_url': self.facebook_url,
            'twitter_url':self.twitter_url,
            'instagram_url':self.instagram_url,
            'formatted_address':self.formatted_address,
            'rating':self.rating,
            'open_now': self.open_now,
            'business_status':self.business_status,
            'rating_count':self.rating_count,
            'lat' : self.lat,
            'lang' : self.lang,
            'closest_vet' : self.closest_vet,
            'pets' : self.pets,
        })
        return serialObj




