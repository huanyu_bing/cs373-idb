from services import db

class Vet(db.Model):
    __tablename__ = 'vet'

    id = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String(), nullable=False)
    image_url = db.Column(db.String())
    is_closed = db.Column(db.Boolean(), unique=False, default=False)
    url = db.Column(db.String())
    review_count = db.Column(db.Integer)
    rating = db.Column(db.Integer)
    city = db.Column(db.String())
    state = db.Column(db.String())
    zip_code = db.Column(db.String())
    display_phone= db.Column(db.String())
    display_address = db.Column(db.String())
    lat = db.Column(db.Float)
    lang = db.Column(db.Float)
    closest_org = db.Column(db.String())
    closest_pets = db.Column(db.String())
    

    def serialize(self):
        serialObj = dict({
            'id': self.id,
	        'name': self.name,
            'image_url' : self.image_url,
            'is_closed' : self.is_closed,
            'url' : self.url,
            'review_count' : self.review_count,
            'rating' : self.rating,
            'city' : self.city,
            'state' : self.state,
            'zip_code' : self.zip_code,
            'display_phone' : self.display_phone,
            'display_address' : self.display_address,
            'lat' : self.lat,
            'lang' : self.lang,
            'closest_org' : self.closest_org,
            'closest_pets' : self.closest_pets,
        })

        return serialObj
