import sys
import requests
import json
import logging
import time
import googlemaps

logging.captureWarnings(True)

org_api_url = "https://api.petfinder.com/v2/organizations?state=TX&limit=100"
MAPS_API_KEY = 'AIzaSyD8kvFlBIG3yIhes2l2A7YiCqgmHdTM9E0'
map_client = googlemaps.Client(MAPS_API_KEY)

# function to obtain a new OAuth 2.0 token from the authentication server
def get_new_token():

	auth_server_url = "https://api.petfinder.com/v2/oauth2/token"
	client_id = 'lfVAazTc1L0d5uq25dYmL9FvOuOcAdphW3yjZKkbtjVHdZY6rp'
	client_secret = 'qJLN2kJPGonaPH8MoZAMT4quOgWLQY6sAvAQ2Jjp'

	token_req_payload = {'grant_type': 'client_credentials'}

	token_response = requests.post(auth_server_url,
	data=token_req_payload, verify=False, allow_redirects=False,
	auth=(client_id, client_secret))
				
	if token_response.status_code !=200:
		print("Failed to obtain token from the OAuth 2.0 server", file=sys.stderr)
		sys.exit(1)

	print("Successfuly obtained a new token")
	tokens = json.loads(token_response.text)
	return tokens['access_token']

def get_place_info(location_name):
    try:
        # location_name = 'Lanxess Arena Köln'
        response = map_client.places(query=location_name)
        results = response.get('results')[0]
        return results
    except Exception as e:
        print(e)
        return None

# obtain a token before calling the API for the first time
token = get_new_token()

# call the API with the token
api_call_headers = {'Authorization': 'Bearer ' + token}
place_list = []
for i in range(1, 5):
	api_call_response = requests.get(org_api_url+"&page="+str(i), headers=api_call_headers)
	api_call_response_dict = api_call_response.json()

	for org in api_call_response_dict["organizations"]:
		place_info = get_place_info(org["name"])
		if place_info != None:
			# print(place_info)
			if "formatted_address" in place_info:
				org["formatted_address"] = place_info["formatted_address"]
			if "rating" in place_info:
				org["rating"] = place_info["rating"]
			if "opening_hours" in place_info:
				org["open_now"] = place_info["opening_hours"]["open_now"]
			else :
				org["open_now"] = False
			if "business_status" in place_info:
				org["business_status"] = place_info["business_status"]
			if "user_ratings_total" in place_info:
				org["rating_count"] = place_info["user_ratings_total"]
			org["lat"] = place_info["geometry"]["location"]["lat"]
			org["lang"] = place_info["geometry"]["location"]["lng"]
	place_list.append(api_call_response_dict)
				
with open('../data/orgs_data.json', 'w') as openfile:
	json.dump(place_list, openfile)

