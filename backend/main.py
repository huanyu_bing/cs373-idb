import os

from services import app
from services import db
from endpoints.orgs import orgs, populate_orgs, populate_pet_relation
from endpoints.vets import vets, populate_vets, populate_vet_relation
from endpoints.pets import pets, populate_pets

app.register_blueprint(vets)
app.register_blueprint(orgs)
app.register_blueprint(pets)

@app.route("/")
def hello_world():
    return "Ready Set Pet!"

def reset_db():
    print('reset db called')
    with app.app_context():
        db.session.remove()
        db.drop_all()
        db.create_all()

# reset_db()
# with app.app_context():
#     db.create_all()
    
# print('create db called')
reset_db()
populate_vets()
populate_orgs()
populate_pets()
populate_pet_relation()
populate_vet_relation()
print('the end')



# api.add_resource(classname, "") 
# the restful library allow the code to be org in an oop way. so a class with get, post 
if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8000)))
    # set debug to False for main!!


