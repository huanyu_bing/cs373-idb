import json
import os
from services import db, app
from flask import Blueprint, render_template, abort, request
import utils
from models.pet import Pet
from endpoints.orgs import get_orgs_by_id, getClosestVet
JSON_PETS = os.path.join(os.path.dirname(__file__), "../data/pet_data.json")

pets = Blueprint("pets",__name__)

def addressToString(address):
    if address != None:
        addressStr = address["address1"] + ", "+ address["city"] + ", " + address["state"] + ", " + address["country"]
        return addressStr
    else :
        return "Address not available."

def get_location(organization_id):
    org = get_orgs_by_id(organization_id)["data"]["organization"]
    # print(org["status"])
    # print("org: " + org["name"])
    return [org["lat"], org["lang"]]


@pets.route("/pop_pets")
def populate_pets():
    print("populating pets")
    json_file = JSON_PETS
    with open(json_file) as jsn:
        pet_data = json.load(jsn)
        with app.app_context():
            for i in range(len(pet_data)):
                for pet in pet_data[i]["animals"]:
                    try:
                        location = get_location(pet["organization_id"])
                        if location[0] == None:
                            continue
                        db_row = dict({
                            'id': pet["id"],
                            'name': pet["name"],
                            'organization_id' : pet["organization_id"],
                            'type': pet["type"],
                            'breeds' : pet["breeds"]["primary"],
                            'age': pet["age"],
                            'gender' : pet['gender'],
                            'size': pet["size"],
                            'status': pet["status"],
                            'email': pet["contact"]["email"],
                            'phone': pet["contact"]["phone"],
                            'address': addressToString(pet["contact"]["address"]),
                            'photo_url': pet["photos"][0]["medium"] if len(pet["photos"]) != 0 else "None",
                            'lat' : location[0],
                            'lang' : location[1],
                            'closest_vet' : getClosestVet(location[0], location[1]),
                        })
                        cur_id = pet["id"]
                        if len(db.session.query(Pet).filter(Pet.id == cur_id).all()) == 0:
                                temp = Pet(**db_row)
                                db.session.add(temp)
                    except:
                        continue
            db.session.commit()

@pets.route("/pets/<id>")
def get_pets_by_id(id):
    ret = utils.get_model_by_id(id, Pet)
    print(ret)
    if ret["status"] == "success":
        ret["data"]["pet"] = ret["data"].pop("instance")
    return ret    

@pets.route("/pets")
def get_pets():
    search_params = [
        Pet.organization_id,
        Pet.name, 
        Pet.email, 
        Pet.phone, 
        Pet.address
    ]
    
    filter_params = [Pet.type, Pet.breeds, Pet.status, Pet.gender]
    filter_names = ["type", "breeds", "status", "gender"]
    sort_params = [Pet.age, Pet.size, Pet.name]
    sort_names = ["age", "size", "name"]
    try:
        page, page_size = utils.parse_models_pagination_params(request.args)
    except Exception as e:
        print(e)
        return {"status": "fail", "data": e.args}
    
    base_query = Pet.query

    print(utils.get_filter_values(request.args, base_query, filter_params, filter_names, Pet.type))
    
    base_query = utils.search_query(request.args, base_query, search_params)
    base_query = utils.filter_query(request.args, base_query, filter_names, filter_params)
    base_query = utils.sort_query(request.args, base_query, sort_names, sort_params, Pet.name)
    ret = utils.get_model_pages(base_query, page, page_size)

    if ret["status"] == "success":
        ret["data"]["pet"] = ret["data"].pop("instances")
    return ret    