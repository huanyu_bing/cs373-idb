import json
import os
from services import db, app
from flask import Blueprint, render_template, abort, request
import utils
from models.vet import Vet
from models.org import Organization
import geopy.distance

JSON_VETS = os.path.join(os.path.dirname(__file__), "../data/vets_data.json")

vets = Blueprint("vets",__name__)

def getClosestOrg(vet_id):
    orgs = Organization.query.all()
    vet = get_vets_by_id(vet_id)["data"]["vet"]
    print(vet)
    min_distance = None
    min_id = None

    for org in orgs:
        distance = geopy.distance.distance(
                (vet["lat"], vet["lang"]),
                (org.lat, org.lang),
            ).miles
        if min_distance == None:
            min_distance = distance
            min_id = org.id
            continue
        if distance < min_distance :
            min_distance = distance
            min_id = org.id
        print("id: " + org.id)
        print("distance: ", distance)
    return min_id

def addressToString(address):
    if address != None:
        addressStr = address[0]
        for i in range(1, len(address)):
            addressStr = addressStr + ", " + address[i]
        return addressStr
    else :
        return "Address not available."

@vets.route("/pop_vets")
def populate_vets():
    print("populating vets")
    json_file = JSON_VETS
    with open(json_file) as jsn:
        vet_data = json.load(jsn)
        for vet in vet_data["businesses"]:
            try:
                db_row = dict({
                    'id': vet["id"],
                    'name': vet["name"],
                    'image_url' : vet["image_url"],
                    'is_closed': vet["is_closed"],
                    'url' : vet["url"],
                    'review_count': vet["review_count"],
                    'rating': vet["rating"],
                    'city': vet["location"]["city"],
                    'state': vet["location"]["state"],
                    'zip_code': vet["location"]["zip_code"],
                    'display_phone': vet["display_phone"],
                    'display_address': vet["location"]["display_address"],
                })
                with app.app_context():
                    cur_id = vet["id"]
                    if len(db.session.query(Vet).filter(Vet.id == cur_id).all()) == 0:
                        temp = Vet(**db_row)
                        db.session.add(temp)
                        db.session.commit()
            except:
                continue

                
@vets.route("/vets/<id>")
def get_vets_by_id(id):
    ret = utils.get_model_by_id(id, Vet)
    print(ret)
    if ret["status"] == "success":
        ret["data"]["vet"] = ret["data"].pop("instance")
    return ret    

@vets.route("/vets")
def get_vets():
    try:
        page, page_size = utils.parse_models_pagination_params(request.args)
    except Exception as e:
        print(e)
        return {"status": "fail", "data": e.args}
    
    base_query = Vet.query
    ret = utils.get_model_pages(base_query, page, page_size)

    if ret["status"] == "success":
        ret["data"]["vet"] = ret["data"].pop("instances")
    return ret            

