import json
import os
from services import db, app
from flask import Blueprint, render_template, abort, request
import utils
from models.vet import Vet
from models.org import Organization
from models.pet import Pet
import geopy.distance

JSON_ORGS = os.path.join(os.path.dirname(__file__), "../data/orgs_data.json")

orgs = Blueprint("orgs",__name__)

def hoursToString(hours):
    if hours["monday"] != None:
        hourStr = ""
        hourStr += "Monday: " + hours["monday"] + "\n"
        hourStr += "Tuesday: " + hours["tuesday"] + "\n"
        hourStr += "Wednesday: " + hours["wednesday"] + "\n"
        hourStr += "Thursday: " + hours["thursday"] + "\n"
        hourStr += "Friday: " + hours["friday"] + "\n"
        hourStr += "Saturday: " + hours["saturday"] + "\n"
        hourStr += "Sunday: " + hours["sunday"] + "\n"
        return hourStr
    else :
        return "Hour info not available."


# returns the id of the closest vet
# @orgs.route("/closestVet")
def getClosestVet(lat, lang):
    vets = Vet.query.all()
    # org = get_orgs_by_id(org_id)["data"]["organization"]
    min_distance = None
    min_id = None

    for vet in vets:
        distance = geopy.distance.distance(
                (lat, lang),
                (vet.lat, vet.lang),
            ).miles
        if min_distance == None:
            min_distance = distance
            min_id = vet.id
            continue
        if distance < min_distance :
            min_distance = distance
            min_id = vet.id

    return min_id

def getPets(org_id):
    pets = Pet.query.filter_by(organization_id=org_id).all()
    return [str(pet.id) for pet in pets]
    

@orgs.route("/pop_pet_relation")
def populate_pet_relation():
    print("populating pet relation")
    with app.app_context():
        for row in Organization.query.all():  # all() is extra
            row.pets = ','.join(getPets(row.id))
        db.session.commit()


@orgs.route("/pop_orgs")
def populate_orgs():
    print("populating orgs")
    json_file = JSON_ORGS
    with open(json_file, encoding='utf-8') as jsn:
        org_data = json.load(jsn)
        with app.app_context():
            for i in range(len(org_data)) :
                for org in org_data[i]["organizations"]:
                    try:
                        db_row = dict({
                            'id' : org["id"],
                            'name': org["name"],
                            'email': org["email"],
                            'phone': org["phone"] if org["phone"] != None else "None",
                            'hours': hoursToString(org["hours"]),
                            'website': org["website"] if org["website"] != None else "None",
                            'photos': org["photos"][0]["medium"] if len(org["photos"]) !=0 else "None",
                            'facebook_url': org["social_media"]["facebook"] if org["social_media"]["facebook"] != None else "None",
                            'twitter_url': org["social_media"]["twitter"] if org["social_media"]["twitter"] != None else "None",
                            'instagram_url': org["social_media"]["instagram"] if org["social_media"]["instagram"] != None else "None",
                            'formatted_address' : org["formatted_address"] ,
                            'rating' : org["rating"],
                            'open_now': org["open_now"],
                            "business_status" : org["business_status"],
                            "rating_count" : org["rating_count"],
                            "lat" : org["lat"],
                            "lang" : org["lang"],
                            'closest_vet': getClosestVet(org["lat"], org["lang"]),
                        })
                        cur_id = org["id"]
                        if len(db.session.query(Organization).filter(Organization.id == cur_id).all()) == 0:
                            temp = Organization(**db_row)
                            db.session.add(temp)
                    except:
                        continue
            db.session.commit()
        


@orgs.route("/orgs/<id>")
def get_orgs_by_id(id):
    ret = utils.get_model_by_id(id, Organization)
    if ret["status"] == "success":
        ret["data"]["organization"] = ret["data"].pop("instance")
    return ret

@orgs.route("/orgs")
def get_orgs():
    search_params = [
        Organization.id, 
        Organization.name, 
        Organization.email, 
        Organization.phone, 
        Organization.formatted_address
    ]
    
    filter_params = [Organization.open_now, Organization.business_status]
    filter_names = ["open_now", "business_status"]
    
    sort_params = [Organization.name, Organization.rating, Organization.rating_count]
    sort_names = ["name", "rating", "rating_count"]

    try:
        page, page_size = utils.parse_models_pagination_params(request.args)
    except Exception as e:
        print(e)
        return {"status": "fail", "data": e.args}

    

    base_query = Organization.query

    print(utils.get_filter_values(request.args, base_query, filter_params, filter_names, Organization.open_now))
    
    base_query = utils.search_query(request.args, base_query, search_params)
    base_query = utils.filter_query(request.args, base_query, filter_names, filter_params)
    base_query = utils.sort_query(request.args, base_query, sort_names, sort_params, Organization.name)
    ret = utils.get_model_pages(base_query, page, page_size)
    if ret["status"] == "success":
        ret["data"]["organization"] = ret["data"].pop("instances")
    return ret       