from services import db
from sqlalchemy import or_, desc
# Some methods are inspire by 
# https://gitlab.com/coleweinman/swe-college-project/-/blob/main/backend/common.py


def parse_request(req_args,name,type,list = False):
    """
    This parse the request argument to the specific type of list requested. 
    """
    if name in req_args:
        try:
            print(name,req_args)
            if list:
                args = req_args[name].split(",")
            else:
                args = [req_args[name]] #mod
            ret = []
            for arg in args:
                ret.append(type(arg))
            if list:
                return ret
            else:
                return ret[0]
        except:
            raise Exception({str(name) + " is invalid"})
    else:
        # raise Exception({str(name) + " is not a request argument"}) #mod
        return None

def filter_query(req_args, base_query, filter_names, filter_fields):
    filters = {}
    for (name, field) in zip(filter_names, filter_fields): 
        result = parse_request(req_args, name + "Filter", str)
        if result is not None:
            filters[field] = result
    
    if len(filters) > 0:
        for filter in filters:
            union_filters = filters[filter].split(",")
            query_filters = []
            for actual_filter in union_filters:
                query_filters.append(filter.contains(filter.type.python_type(actual_filter)))
            base_query = base_query.filter(or_(*query_filters))
    return base_query

def sort_query(req_args, base_query, sort_names, sort_fields, default):
    sort_field = parse_request(req_args, "sortFilter", str) 
    ordering_field = parse_request(req_args, "ascending", str)
    actual_field = default
    if sort_field is not None:
        for (name, field) in zip(sort_names,sort_fields):
            if(sort_field == name):
                actual_field = sort_field
                break        
    if ordering_field == "false":
        return base_query.order_by(desc(actual_field))
    return base_query.order_by(actual_field)

def search_query(req_args, base_query, search_fields):
    query = parse_request(req_args, "searchFilter", str)

    if(query is None):
        return base_query

    search_args = []
    for field in search_fields:
        search_args.append(field.ilike("%" + query + "%"))
    return base_query.filter(or_(*search_args))

def get_filter_values(req_arg, base_query, filter_params, filter_names, default_filter):
    query = parse_request(req_arg, "distinctFilter", str)
    if(query is None):
        return "none"
    actualFilter = default_filter
    for (name, field) in zip(filter_names, filter_params):
        if(name == query):
            actualFilter = field
            break
    distinctValues = []
    for value in db.session.query(actualFilter).distinct():
        distinctValues.append(value[0])
    
    return distinctValues

def paginate_request(base_query, page, page_size):
    return base_query.paginate(page = page, per_page = page_size)

def parse_models_pagination_params(req_args):
    page = parse_request(req_args, "page", int) or 1
    page_size = parse_request(req_args, "pageSize", int) or 12
    return page, page_size

def get_model_pages(base_query,page,page_size):
    try:
        results = paginate_request(base_query, page, page_size)
        instances = [inst.serialize() for inst in results.items]
    except Exception as e:
        print(e)
        return {"status": "error", "message": "Data fetch failed."}
    return {
        "status": "success",
        "data": {
            "page": page,
            "pageSize": page_size,
            "results": len(instances),
            "total": results.total,
            "instances": instances,
        },
    }

def get_model_by_id(id, model):
    try:
        instance = model.query.get(id)
    except:
        return {"status": "fail", "data": {"id": "Invalid ID"}}
    if instance == None:
        return {"status": "error", "message": "ID not find."}
    return {"status": "success", "data": {"instance": (instance.serialize())}}
