#temp file to test backend
from unittest import main, TestCase
import utils
from models.org import Organization
from endpoints.orgs import get_orgs, get_orgs_by_id
from endpoints.pets import get_pets, get_pets_by_id
from endpoints.vets import get_vets, get_vets_by_id
from services import app



class UnitTests(TestCase):
    # ----Organization Endpoint test
    def test_1(self):
        requrl = "/orgs"
        with app.test_request_context(requrl):
            ret = get_orgs()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertLessEqual(ret["data"]["results"], 9)
        self.assertEqual(ret["data"]["pageSize"], 9)
    # 
    def test_2(self):
        requrl = "/orgs"
        with app.test_request_context(requrl):
            ret = get_orgs_by_id('TX1806')
        self.assertEqual(ret["status"], "success")

    def test_3(self):
        requrl = "/pets"
        with app.test_request_context(requrl):
            ret = get_pets()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertLessEqual(ret["data"]["results"], 9)
        self.assertEqual(ret["data"]["pageSize"], 9)

    def test_4(self):
        requrl = "/pets"
        with app.test_request_context(requrl):
            ret = get_pets_by_id(58556110)
        self.assertEqual(ret["status"], "success")

    def test_5(self):
        requrl = "/vets"
        with app.test_request_context(requrl):
            ret = get_vets()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertLessEqual(ret["data"]["results"], 9)
        self.assertEqual(ret["data"]["pageSize"], 9)
    
    def test_6(self):
        requrl = "/vets"
        with app.test_request_context(requrl):
            ret = get_vets_by_id("rFAYi2rUiWYuvfSRbvWddg")
        self.assertEqual(ret["status"], "success")

    def test_7(self):
        requrl = "/orgs?page=2&pageSize=10"
        with app.test_request_context(requrl):
            ret = get_orgs()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 2)
        self.assertLessEqual(ret["data"]["results"], 10)
        self.assertEqual(ret["data"]["pageSize"], 10)


if __name__ == "__main__":
    # ret = utils.get_model_by_id('TX248',Organization)
    # requrl = "/orgs?page=2&pageSize=10"
    # with app.test_request_context(requrl):
    #     ret = get_orgs()
    # print(ret)
    # print(ret)
    main()







