.DEFAULT_GOAL := all
SHELL         := bash

FRONTEND_DIR = frontend

all:

install:
	(cd $(FRONTEND_DIR) && npm install)

run-script:
	(cd $(FRONTEND_DIR) && npm run-script build)

start:
	(cd $(FRONTEND_DIR) && npm start)

run:
	$(MAKE) install
	$(MAKE) start

