import React from 'react';
import {Container, Nav } from "react-bootstrap";
import logo from './logo.svg';
import './App.css';
import Navbar from "./pages/home/Header";
import Home from "./pages/home/home";
import About from "./pages/about/about";
import Pets from "./pages/pets/pets";
import Organizations from "./pages/organizations/organization";
import Vets from "./pages/vets/vets";
import {Route, Router, Routes } from "react-router-dom";
import OrgsInstancePage from "./pages/organizations/organizationInstance";
import VetsInstancePage from "./pages/vets/vetInstancePage";
import PetsInstancePage from "./pages/pets/petInstnace";
import Visualization from "./pages/visulization/Visualization";
import RelifMapVisualization from "./pages/visulization/RelifMapVisualization";
import SearchPage from "./pages/search/SearchPage";

function App() {
  return (
    <div>
    {/* <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/pets">Pets</Nav.Link>
            <Nav.Link href="/organizations">Organizations</Nav.Link>
            <Nav.Link href="/vets">Vets</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar> */}
    {/* <Router> */}
      <Navbar />
      <Routes>
      <Route path = "/" element = {<Home/>}/>
      <Route path = "/about" element = {<About/>}/>
      <Route path = "/pets" element = {<Pets/>}/>
      <Route path = "/organizations" element = {<Organizations/>}/>
      <Route path = "/vets" element = {<Vets/>}/>
      <Route path="/orgs/:OrgId" element={<OrgsInstancePage />} />
      <Route path="/vets/:VetId" element={<VetsInstancePage />} />
      <Route path="/pets/:PetId" element={<PetsInstancePage />} />
      <Route path="/visualizations" element={<Visualization />} />
      <Route path="/RelifMapsVisualization" element={<RelifMapVisualization />} />

      <Route path="/SearchPage" element={<SearchPage />} />



      </Routes>
    {/* </Router> */}
    </div>
  );
}

export default App;

//random comment to commit since aws doesn't update unless you commit part 2
 {/* <Route path = "/petinstance0" element = {<Pet0/>}/>
      <Route path = "/petinstance1" element = {<Pet1/>}/>
      <Route path = "/petinstance2" element = {<Pet2/>}/>
      <Route path = "/vetinstance0" element = {<Vet0/>}/>
      <Route path = "/vetinstance1" element = {<Vet1/>}/>
      <Route path = "/vetinstance2" element = {<Vet2/>}/> */}
  
