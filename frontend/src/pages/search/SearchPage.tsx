// The following code is inspire from https://gitlab.com/coleweinman/swe-college-project/-/blob/main/frontend/src/components/search/SearchPage.tsx

import React, {useState, useEffect} from 'react';
import { Container, Stack, Typography } from "@mui/material";
import axios, { AxiosResponse } from "axios";
import { useNavigate, useSearchParams } from "react-router-dom";
import Pet from "../pets/petsInterface";
import PetCard from "../pets/PetCard";
import Vet from "../vets/vetsInterface";
import VetCard from "../vets/VetCard";
// import LoadingWidget from "../common/LoadingWidget";
// import { stateOptions } from "../common/states";
import Org from "../organizations/orgInterface";
import OrgCard from "../organizations/OrganizationCard";
import { API_URL } from "../../global";
import { ExactFilter, ModelToolbar } from "../../toolbar/ModelToolbar";
import ViewAllBar from "./ViewAllBar";
import Spinner from 'react-bootstrap/Spinner';


function SearchPage(props: any) {
  const [searchParams] = useSearchParams();
  let [vets, setVets] = useState<Vet[] | null>(null);
  let [pets, setPets] = useState<Pet[] | null>(null);
  let [orgs, setOrgs] = useState<Org[] | null>(null);
  const [totalVets, setTotalVets] = useState<number>(0);
  const [totalPets, setTotalPets] = useState<number>(0);
  const [totalOrgs, setTotalOrgs] = useState<number>(0);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchVets = async () => {
      setVets(null);
      let response: AxiosResponse<any, any> = await axios.get(
        `${API_URL}/vets?` +
          searchParams.toString() +
          "&pageSize=6&required=description,thumbnailUrl"
      );
      let data = response.data["data"];
      setVets(data["vet"]);
      setTotalVets(data["total"]);
    };

    const fetchPets = async () => {
      setPets(null);
      let response: AxiosResponse<any, any> = await axios.get(
        `${API_URL}/pets?` + searchParams.toString() + "&pageSize=6"
      );
      let data = response.data["data"];
      setPets(data["pet"]);
      setTotalPets(data["total"])
    };

    const fetchOrgs = async () => {
      setOrgs(null);
      let response: AxiosResponse<any, any> = await axios.get(
        `${API_URL}/orgs?` + searchParams.toString() + "&pageSize=6"
      );
      let data = response.data["data"];
      setOrgs(data["organization"]);
      setTotalOrgs(data["total"]);
    };
    fetchVets();
    fetchPets();
    fetchOrgs();
  }, [searchParams]);

  const onViewAll = (link: string) => {
    navigate(link + "?" + searchParams);
  };

  // const exactFilters: ExactFilter[] = [
  //   {
  //     label: "State",
  //     field: "state",
  //     options: stateOptions,
  //   },
  // ];

  return (
    <Container
      className="page-container"
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Typography
        gutterBottom
        className="modelTitle"
        variant="h2"
        sx={{ textAlign: "center" }}
      >
        Search
      </Typography>
      <ModelToolbar
        exactFilters={[]}
        sortOptions={[]}
        sortAscending={true}
      />
      <Typography
        variant="h3"
        sx={{ padding: "24px 8px 8px 8px", textAlign: "center" }}
      >
        Vets
      </Typography>
      {vets === null && <Spinner animation="border"/>}
      {vets !== null && (
        <Stack direction="row" flexWrap="wrap">
        {vets.map((val) => (<VetCard 
          pos = {val.id}
          id = {val.id}
          name = {val.name}
          location = {val.display_address}
          photos = {val.image_url}
          rating = {val.rating}
          query = {searchParams.get("searchFilter") ?? undefined}
          review_count = {val.review_count}
          closed = {val.is_closed}
          phone = {val.display_phone}
        ></VetCard>))}
      </Stack>
      )}
      <ViewAllBar
        link="/vets"
        total={totalVets}
        onViewAll={onViewAll}
      />
      <Typography
        variant="h3"
        sx={{ padding: "24px 8px 8px 8px", textAlign: "center" }}
      >
        Pets
      </Typography>
      {pets === null && <Spinner animation="border"/>}
      {pets !== null && (
            <Stack direction="row" flexWrap="wrap">
                {pets.map((val) => (<PetCard 
                        pos = {val.id}
                        id = {val.id}
                        organization = {val.organization_id}
                        type = {val.type}
                        query = {searchParams.get("searchFilter") ?? undefined}
                        // colors = {val.colors}
                        species = {val.type}
                        age = {val.age}
                        gender = {val.gender}
                        size = {val.size}
                        name = {val.name}
                        photos = {val.photo_url} //(val.photo_url == "None") ? whitesquare :
            ></PetCard>))}
        </Stack>
      )}
      <ViewAllBar link="/pets" total={totalPets} onViewAll={onViewAll} />
      <Typography
        variant="h3"
        sx={{ padding: "24px 8px 8px 8px", textAlign: "center" }}
      >
        Organizations
      </Typography>
      {orgs !== null && (
                    <Stack direction="row" flexWrap="wrap">
                      {orgs.map((val) => (<OrgCard
                        pos = {val.id}
                        id = {val.id}
                        name = {val.name}
                        email = {val.email}
                        address = {val.formatted_address}
                        query = {searchParams.get("searchFilter") ?? undefined}
                        media = {val.instagram_url}
                        phone = {val.phone}
                        photos = {val.photos}
                        statement = {val.open_now}
                      ></OrgCard>))}
                    </Stack>
      )}
      <ViewAllBar
        link="/companies"
        total={totalOrgs}
        onViewAll={onViewAll}
      />
    </Container>
  );
}

export default SearchPage;
