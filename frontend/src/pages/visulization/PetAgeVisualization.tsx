import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { API_URL } from "../../global";
import {Button, Card, Stack } from 'react-bootstrap';
import { TextField, MenuItem } from "@mui/material";

import React, { PureComponent } from 'react';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip, Legend } from 'recharts';
import Pet from "../pets/petsInterface";
import Spinner from 'react-bootstrap/Spinner';
import attributes from "../pets/petOtherOption";


function PetAgeVisualization(props: any){
    let [currentState, setCurrentState] = React.useState<string>("Dog");
    let [divider, setdivider] = React.useState<string>("gender");
    let [data, setData] = useState<any[]>([]);
    let visualization =  <Spinner animation="border"/>;
    useEffect(() => {
    const getData = async () => {
      let petResponse: AxiosResponse<any, any> = await axios.get(
        `${API_URL}/pets?pageSize=400&typeFilter=${currentState}`
      );
      let pets = petResponse.data["data"]["pet"];
      let data = process_data(pets,divider);
      // console.log(data)
      setData(data);
    };
    getData().then(() => console.log("loaded"));
  }, [currentState,divider]);
  if (data.length > 0){
    visualization = pie_chart(data);
  }
  return (
     <Card style={{ width: 'auto' }}>
        <Card.Body>
       <h1> Number of Pets in each type</h1>
        </Card.Body>
         <Card.Body>
        <TextField
        id="filter-field"
        select
        label="Type"
        value={currentState}
        onChange={(event) => setCurrentState(event.target.value)}
        InputProps={{
          sx: {
            borderRadius: "8px",
            flexGrow: 1,
            minWidth: "150px",
            display: "flex",
            textAlign: "start",
          },
        }}
      >
        {attributes.Type.map((option) => (
          <MenuItem key={option} value={option}>
            {option}
          </MenuItem>
        ))}
        </TextField>
        <TextField
        id="filter-field"
        select
        label="Feature"
        value={divider}
        onChange={(event) => setdivider(event.target.value)}
        InputProps={{
          sx: {
            borderRadius: "8px",
            flexGrow: 1,
            minWidth: "150px",
            display: "flex",
            textAlign: "start",
          },
        }}
      >
        {attributes.Other.map((option) => (
          <MenuItem key={option} value={option}>
            {option}
          </MenuItem>
        ))}
        </TextField>
        </Card.Body>
        <Card.Body>
            {visualization}
        </Card.Body>
     </Card>
  );
}

const process_data = (pets: Pet[], divider) =>{
    var record: {[key:string]:number} = {};
    for (const pet of pets){
        record[pet[divider]] = record[pet[divider]] + 1 || 1;
    }
    var result : any [] =[];
    for (const[key,val] of Object.entries(record)){
        result.push({
            name: key,
            value: val,
        })
    }
    return result
}
// this function is from https://recharts.org/en-US/examples/PieChartWithCustomizedLabel
const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};
// This funciton is from https://www.tutorialspoint.com/reactjs-how-to-create-a-pie-chart-using-recharts
const CustomTooltip = (props:any) => {
    const {active, payload, label} = props;
      if (active) {
         return (
         <div
            className="custom-tooltip"
            style={{
              //  backgroundColor: "#ffff",
               padding: "5px",
               border: "1px solid #cccc"
            }}
         >
            <label>{`${payload[0].name} : ${payload[0].value}`}</label>
         </div>
      );
   }
   return null;
};

const pie_chart = (data: any[]) =>{
    var COLORS = ["#8884d8", "#82ca9d", "#FFBB28", "#FF8042", "#AF19FF"];
    return (
    <ResponsiveContainer  height={300}>
    <PieChart width={730} height={300}>
      <Pie
         data={data}
         color="#000000"
         dataKey="value"
         nameKey="name"
         label={renderCustomizedLabel}
         cx="50%"
         cy="50%"
         outerRadius={120}
         fill="#8884d8"
      >
        {data.map((entry, index) => (
              <Cell
               key={`cell-${index}`}
               fill={COLORS[index % COLORS.length]}
            />
            ))}
    </Pie>
    <Tooltip content={<CustomTooltip/>} />
    <Legend />
    </PieChart>
    </ResponsiveContainer>
    );
};
export default PetAgeVisualization;