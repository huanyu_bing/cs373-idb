import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { API_URL } from "../../global";
import {Button, Card, Stack } from 'react-bootstrap';
import React, { PureComponent } from 'react';
// import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip, Legend } from 'recharts';
import {
   AreaChart,
   Area,
   YAxis,
   XAxis,
   CartesianGrid,
   Tooltip,
   Legend
   } from "recharts";

import Org from "../organizations/orgInterface";
import Spinner from 'react-bootstrap/Spinner';

function OrgVisualization(props: any){
    let [data, setData] = useState<any[]>([]);
    useEffect(() => {
    if (data.length === 0) {
      const getData = async () => {
        let orgResponse: AxiosResponse<any, any> = await axios.get(
          `${API_URL}/orgs?pageSize=400`
        );
        let orgs = orgResponse.data["data"]["organization"];
        console.log(orgs)
        let data = process_data(orgs);
        console.log(data)
        setData(data);
      };
      getData().then(() => console.log("loaded"));
    }
  }, [data]);

  let visualization =  <Spinner animation="border"/>;
  if (data.length > 0){
    visualization = area_chart(data);
  }



  return (
     <Card style={{ width: 'auto' }}>
        <Card.Body>
        <h1>Reviews of organizations</h1>
        </Card.Body>
        <Card.Body style={{ width: 'auto' }}>
            {visualization}
        </Card.Body>
     </Card>
  );
}

const process_data = (orgs: Org[]) =>{
    var record: {[key:string]:number} = {};
    for (const org of orgs){
        record[org.rating] = record[org.rating] + 1 || 1;
    }
    var result : any [] =[];
    for (const[key,val] of Object.entries(record)){
        result.push({
            name: key,
            value: val,
        })
    }
    return result
}
// this function is from https://recharts.org/en-US/examples/PieChartWithCustomizedLabel
const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};
// This funciton is from https://www.tutorialspoint.com/reactjs-how-to-create-a-pie-chart-using-recharts
const CustomTooltip = (props:any) => {
    const {active, payload, label} = props;
      if (active) {
         return (
         <div
            className="custom-tooltip"
            style={{
               backgroundColor: "#ffff",
               padding: "5px",
               border: "1px solid #cccc"
            }}
         >
            <label>{`${payload[0].name} : ${payload[0].value}`}</label>
         </div>
      );
   }
   return null;
};
//This function is based on https://www.tutorialspoint.com/how-to-create-an-area-chart-using-recharts-in-reactjs
const area_chart = (data: any[]) =>{
    return (
      <AreaChart
               width={730}
               height={250}
               data={data}
               margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
            <defs>
               <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
                  <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
               </linearGradient>
               <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8} />
                  <stop offset="95%" stopColor="#82ca9d" stopOpacity={0} />
               </linearGradient>
            </defs>
            <XAxis dataKey="name" />
            <YAxis />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip />
            <Legend />
            <Area
               type="monotone"
               dataKey="value"
               stroke="#8884d8"
               fillOpacity={1}
               fill="url(#colorUv)"/>
            </AreaChart>
    );
};
export default OrgVisualization;