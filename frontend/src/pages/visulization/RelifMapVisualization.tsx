import PetVisualization from "./PetVisualization"
import CountryVisualization from "./CountryVisualization";
import CrisesCountryVisulization from "./CrisesCountryVisulization";
import CharityVisualzation from "./CharitypieVisualization";
import MetaTags from 'react-meta-tags';

import {Button, Card, Stack } from 'react-bootstrap';
export default function ReliefMapVisualization(props: any) {
    return(
        <div>
            <MetaTags>
            <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"/>
            </MetaTags>
            <div><CountryVisualization/></div>
            <div><CrisesCountryVisulization/></div>
            <div><CharityVisualzation/></div>
        </div>

    )
}

