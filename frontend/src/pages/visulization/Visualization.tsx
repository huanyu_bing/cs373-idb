import PetVisualization from "./PetVisualization"
import PetAgeVisualization from "./PetAgeVisualization"
import BreedsVisualization from "./BreedsVisualization"
import OrgVisualization from "./OrgVisualization"


import {Button, Card, Stack } from 'react-bootstrap';
export default function Visualization(props: any) {
    return(
        <div>
            <div><PetVisualization/></div>
            <div><PetAgeVisualization/></div>
            <div><BreedsVisualization/></div>
            <div><OrgVisualization/></div>
        </div>

    )
}

