import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { API_URL } from "../../global";
import {Button, Card } from 'react-bootstrap';
import { Stack, TextField, MenuItem, Box } from "@mui/material";
import React, { PureComponent } from 'react';
import {
  Legend,
  ResponsiveContainer,
  RadarChart,
  Radar,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
} from "recharts";


import Spinner from 'react-bootstrap/Spinner';
import attributes from "./crisesOption";



interface CountryProps{
    ID: string;
    authorLogo: string;
    country: string;
    dateLastUpdated: string;
    issueType: string;
    latitude: string;
    longitude: string;
    name: string;
    recentCountryReportLink : string;
    reportAuthor : string;
    reportCover : string;
    source : string;
    startingDate : string;
    status : string;
}


function CountryVisualization(props: any){
    let [currentState, setCurrentState] = React.useState<string>("Somalia");
    let [data, setData] = useState<any[]>([]);
    let visualization =  <Spinner animation="border"/>;
    let result: any[] = [];
    useEffect(() => {
      const getData = async () => {
        for (const crises_issue of attributes.crises_issuetype){
          let crisesResponse: AxiosResponse<any, any> = await axios.get(
          ` https://rmback-env.eba-4rfwkmin.us-east-1.elasticbeanstalk.com/crises/?pageSize=400&country=${currentState}&issueType=${crises_issue}`);
          let crises_count = crisesResponse.data.length;
          result.push({category: crises_issue, v:crises_count , fullMark: 5 });
        }
        console.log(result)
        setData(result);        
      };
      getData().then(() => console.log("loaded"));
        }, [currentState]);
        if (data.length > 0){
        visualization = radar_chart(data);
        }
  return (
     <Card style={{ width: 'auto' }}>
        <Card.Body>
        <h1>Number of Crises in each country</h1>
        </Card.Body>
        <Card.Body>
        <TextField
        id="filter-field"
        select
        label="Region"
        value={currentState}
        onChange={(event) => setCurrentState(event.target.value)}
        InputProps={{
          sx: {
            borderRadius: "8px",
            flexGrow: 1,
            minWidth: "150px",
            display: "flex",
            textAlign: "start",
          },
        }}
      >
        {attributes.crises_countries.map((option) => (
          <MenuItem key={option} value={option}>
            {option}
          </MenuItem>
        ))}
        </TextField>
        </Card.Body>
        <Card.Body>
            {visualization}
        </Card.Body>
     </Card>
  );
}

// const process_data = (countries: CountryProps[]) =>{
//     let result :any[] = [];
//     for (let country of countries) {
//         result.push({name: country.name, population: country.population});
//     }
    // return result;
// }

const radar_chart = (data: any[]) =>{
    return(
        <ResponsiveContainer width="100%" height={300}>
          <RadarChart outerRadius={90} width={730} height={250} data={data}>
            <PolarGrid />
            <PolarAngleAxis dataKey="category" />
            <PolarRadiusAxis angle={18} domain={[-1, 5]} />
            <Radar
              name="Frequency"
              dataKey="v"
              stroke="#8884d8"
              fill="#8884d8"
              fillOpacity={0.6}
            />
            <Legend />
          </RadarChart>
        </ResponsiveContainer>

    );
}

export default CountryVisualization;