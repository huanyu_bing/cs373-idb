import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { API_URL } from "../../global";
import {Button, Card } from 'react-bootstrap';
import { Stack, TextField, MenuItem, Box } from "@mui/material";
import React, { PureComponent } from 'react';
import {
  Bar,
  BarChart,
  CartesianGrid,
  Label,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

import Spinner from 'react-bootstrap/Spinner';
import country_areas from "./country";



interface CountryProps{
    area: string;
    borderedBy: string;
    capital: string;
    capitalLatLong: string;
    flag: string;
    interactiveMap: string;
    name: string;
    population: number;
    unMember: boolean;
}


function CountryVisualization(props: any){
    let [currentState, setCurrentState] = React.useState<string>("Caribbean");
    let [data, setData] = useState<any[]>([]);
    let visualization =  <Spinner animation="border"/>;
    useEffect(() => {
      const getData = async () => {
        let countryResponse: AxiosResponse<any, any> = await axios.get(
          ` https://rmback-env.eba-4rfwkmin.us-east-1.elasticbeanstalk.com/countries/?pageSize=400&area=${currentState}`
        );
        let countries = countryResponse.data;
        let data = process_data(countries);
        // console.log(data)
        setData(data);
      };
      getData().then(() => console.log("loaded"));
        }, [currentState]);
        if (data.length > 0){
        visualization = bar_chart(data);
        }
  return (
     <Card style={{ width: 'auto' }}>
        <Card.Body>
        <h1>Population in each country</h1>
        </Card.Body>
        <Card.Body>
        <TextField
        id="filter-field"
        select
        label="Region"
        value={currentState}
        onChange={(event) => setCurrentState(event.target.value)}
        InputProps={{
          sx: {
            borderRadius: "8px",
            flexGrow: 1,
            minWidth: "150px",
            display: "flex",
            textAlign: "start",
          },
        }}
      >
        {country_areas.map((option) => (
          <MenuItem key={option} value={option}>
            {option}
          </MenuItem>
        ))}
        </TextField>
        </Card.Body>
        <Card.Body>
            {visualization}
        </Card.Body>
     </Card>
  );
}

const process_data = (countries: CountryProps[]) =>{
    let result :any[] = [];
    for (let country of countries) {
        result.push({name: country.name, population: country.population});
    }
    return result;
}

const bar_chart = (data: any[]) =>{
    return(
        <ResponsiveContainer width="100%" height={500}>
        <BarChart
          data={data}
          margin={{
            top: 20,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        //   onClick={(e) => {
        //     let index = e["activeTooltipIndex"]!;
        //     props.navigate(`/companies?industriesFilter=${industries[index]}`);
        //   }}
        >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" tick={false}>
            <Label
              value="Country"
              position="insideBottom"
              style={{ textAnchor: "middle" }}
            />
          </XAxis>
          <YAxis>
            <Label
              angle={-90}
              value="Population"
              position="insideLeft"
              style={{ textAnchor: "middle" }}
            />
          </YAxis>
          {/* <Tooltip payload={data} /> */}
          <Bar dataKey="population" fill="#0000FF" />
        </BarChart>
        </ResponsiveContainer>
    );
}

export default CountryVisualization;