import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { API_URL } from "../../global";
import {Button, Card } from 'react-bootstrap';
import { Stack, TextField, MenuItem, Box } from "@mui/material";
import React, { PureComponent } from 'react';
import Pet from "../pets/petsInterface";

import {
  Bar,
  BarChart,
  CartesianGrid,
  Label,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

import Spinner from 'react-bootstrap/Spinner';
import country_areas from "./country";



interface CountryProps{
    area: string;
    borderedBy: string;
    capital: string;
    capitalLatLong: string;
    flag: string;
    interactiveMap: string;
    name: string;
    population: number;
    unMember: boolean;
}


// function PetVisualization(props: any){
//     let [data, setData] = useState<any[]>([]);
//     useEffect(() => {
//     if (data.length === 0) {
//       const getData = async () => {
//         let petResponse: AxiosResponse<any, any> = await axios.get(
//           `${API_URL}/pets?pageSize=400`
//         );
//         let pets = petResponse.data["data"]["pet"];
//         let data = process_data(pets);
//         // console.log(data)
//         setData(data);
//       };
//       getData().then(() => console.log("loaded"));
//     }
//   }, [data]);

//   let visualization =  <Spinner animation="border"/>;
//   if (data.length > 0){
//     visualization = pie_chart(data);
//   }


function BreedsVisualization(props: any){
    // let [currentState, setCurrentState] = React.useState<string>("Caribbean");
    let [data, setData] = useState<any[]>([]);
    let visualization =  <Spinner animation="border"/>;
    useEffect(() => {
      const getData = async () => {
        let breedResponse: AxiosResponse<any, any> = await axios.get(
          `${API_URL}/pets?pageSize=400`
        );
        let breeds = breedResponse.data["data"]["pet"];
        let data = process_data(breeds);
        // console.log(data)
        setData(data);
      };
      getData().then(() => console.log("loaded"));
        }, [data]);
        if (data.length > 0){
        visualization = bar_chart(data);
        }
  return (
     <Card style={{ width: 'auto' }}>
        <Card.Body>
        <h1>Breeds distribution</h1>
        </Card.Body>
        <Card.Body>
            {visualization}
        </Card.Body>
     </Card>
  );
}

const process_data = (pets: Pet[]) =>{
    var record: {[key:string]:number} = {};
    for (const pet of pets){
        record[pet.breeds] = record[pet.breeds] + 1 || 1;
    }
    var result : any [] =[];
    for (const[key,val] of Object.entries(record)){
        result.push({
            name: key,
            population: val,
        })
    }
    return result
}


// const process_data = (pets: Pet[]) =>{
//     let result :any[] = [];
//     for (let pet of pets) {
//         result.push({name: pet.breeds, population: country.population});
//     }
//     return result;
// }

const bar_chart = (data: any[]) =>{
    return(
        <ResponsiveContainer width="100%" height={500}>
        <BarChart
          data={data}
          margin={{
            top: 20,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        //   onClick={(e) => {
        //     let index = e["activeTooltipIndex"]!;
        //     props.navigate(`/companies?industriesFilter=${industries[index]}`);
        //   }}
        >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" tick={false}>
            <Label
              value="Country"
              position="insideBottom"
              style={{ textAnchor: "middle" }}
            />
          </XAxis>
          <YAxis>
            <Label
              angle={-90}
              value="Population"
              position="insideLeft"
              style={{ textAnchor: "middle" }}
            />
          </YAxis>
          {/* <Tooltip payload={data} /> */}
          <Bar dataKey="population" fill="#0000FF" />
        </BarChart>
        </ResponsiveContainer>
    );
}

export default BreedsVisualization;