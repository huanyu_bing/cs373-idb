import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import {Button, Card, Stack } from 'react-bootstrap';
import React, { PureComponent } from 'react';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer, Tooltip, Legend } from 'recharts';
import Spinner from 'react-bootstrap/Spinner';



interface CharityProps{
    donationURL: string;
    latitude: number;
    longitude: number;
    need: string;
    numDonations: string;
    orgMission: string;
    orgName: string;
    orgURL: string;
    projCountry: string;
    projImage: string;
    projSummary: string;
    projTheme: string;
    projTitle: number;
    projectID: boolean;
};



function CharitypieVisualization(props: any){
    let [data, setData] = useState<any[]>([]);
    useEffect(() => {
    if (data.length === 0) {
      const getData = async () => {
        let charResponse: AxiosResponse<any, any> = await axios.get(
          ` https://rmback-env.eba-4rfwkmin.us-east-1.elasticbeanstalk.com/charities?pageSize=400`
        );
        let charities = charResponse.data;
        
        
        let data = process_data(charities);
        data.sort(function(first, second) {
        return second.value - first.value;
        });
        console.log(data)

        setData(data.slice(0,10));
      };
      getData().then(() => console.log("loaded"));
    }
  }, [data]);

  let visualization =  <Spinner animation="border"/>;
  if (data.length > 0){
    visualization = pie_chart(data);
  }
  return (
     <Card style={{ width: 'auto' }}>
        <Card.Body>
        <h1>Ten most frequent Charities project theme distribution</h1>
        </Card.Body>
        <Card.Body>
            {visualization}
        </Card.Body>
     </Card>
  );
}

const process_data = (charities: CharityProps[]) =>{
    var record: {[key:string]:number} = {};
    for (const char of charities){
        record[char.projTheme] = record[char.projTheme] + 1 || 1;
    }
    var result : any [] =[];
    for (const[key,val] of Object.entries(record)){
        result.push({
            name: key,
            value: val,
        })
    }
    return result
}
// this function is from https://recharts.org/en-US/examples/PieChartWithCustomizedLabel
const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};
// This funciton is from https://www.tutorialspoint.com/reactjs-how-to-create-a-pie-chart-using-recharts
const CustomTooltip = (props:any) => {
    const {active, payload, label} = props;
      if (active) {
         return (
         <div
            className="custom-tooltip"
            style={{
               backgroundColor: "#ffff",
               padding: "5px",
               border: "1px solid #cccc"
            }}
         >
            <label>{`${payload[0].name} : ${payload[0].value}`}</label>
         </div>
      );
   }
   return null;
};

const pie_chart = (data: any[]) =>{
    var COLORS = ["#8884d8", "#82ca9d", "#FFBB28", "#FF8042", "#AF19FF"];
    return (
      <ResponsiveContainer  height={300}>
    <PieChart width={730} height={300}>
      <Pie
         data={data}
         color="#000000"
         dataKey="value"
         nameKey="name"
         label={renderCustomizedLabel}
         cx="50%"
         cy="50%"
         outerRadius={120}
         fill="#8884d8"
      >
        {data.map((entry, index) => (
              <Cell
               key={`cell-${index}`}
               fill={COLORS[index % COLORS.length]}
            />
            ))}
    </Pie>
    <Tooltip content={<CustomTooltip/>} />
    <Legend />
    </PieChart>
    </ResponsiveContainer>
    );
};
export default CharitypieVisualization;