import React, { useState, useEffect } from 'react';
import { Link, NavLink } from 'react-router-dom';
import './Header.css';

function Navbar() {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  return (
    <>
      <nav className='navbar'>
        <div className='navbar-container'>
          <NavLink to='/' className='navbar-logo' onClick={closeMobileMenu}>
            ReadySetPets
          </NavLink>
          <ul className={click ? 'nav-menu active' : 'nav-menu'}>
            <li className='nav-item'>
              <NavLink
                to='/'
                className='nav-links'
                onClick={closeMobileMenu}
              >
                Home
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink
                to='/Pets'
                className={({ isActive }) => isActive ? 'nav-links2':'nav-links'}
                onClick={closeMobileMenu}
              >
                Pets
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink 
                to='/Vets' 
                className={({ isActive }) => isActive ? 'nav-links2':'nav-links'}
                onClick={closeMobileMenu}>
                Vets
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink
                to='/Organizations'
                className={({ isActive }) => isActive ? 'nav-links2':'nav-links'}
                onClick={closeMobileMenu}
              >
                Organizations
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink
                to='/About'
                className={({ isActive }) => isActive ? 'nav-links2':'nav-links'}
                onClick={closeMobileMenu}
              >
                About
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink
                to='/visualizations'
                className={({ isActive }) => isActive ? 'nav-links2':'nav-links'}
                onClick={closeMobileMenu}
              >
                Visualizations
              </NavLink>
            </li>
            <li className='nav-item'>
              <NavLink
                to='/RelifMapsVisualization'
                className={({ isActive }) => isActive ? 'nav-links2':'nav-links'}
                onClick={closeMobileMenu}
              >
                ReliefMaps Visualization
              </NavLink>
              </li>
              <li className='nav-item'>
              <NavLink
                to='/SearchPage'
                className={({ isActive }) => isActive ? 'nav-links2':'nav-links'}
                onClick={closeMobileMenu}
              >
                Search
              </NavLink>
              </li>
          </ul>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
