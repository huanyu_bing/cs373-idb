import React from 'react';
import '../../Components/Cards.css';
import CardItem from '../../Components/CardItem';
import home_img from '../../assets/dog_run.jpg';
import findpet from '../../assets/findPet.jpg';
import findVet from '../../assets/findVet.jpg';
import findOrg from '../../assets/findOrg.jpg';


// frontend/src/assets/findPet.PNG
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const homeCard = () => {
  return(
    <div className='cards'>
      <h2>Check out these Pages!</h2>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
          {/* <Row xs={1} md={2} lg={3}>
            <Col> */}
            <CardItem
              src = '../assets/findPet.JPG'
              text='Find your new best friend'
              label='Best Friend'
              path = {findpet}
              link = '/pets'
            />
            {/* </Col>
            <Col> */}
            <CardItem
              src = '../assets/dog_run.jpg'
              text='Vets'
              label='Care'
              path = {findVet}
              link = '/vets'
            />
            {/* </Col>
            <Col> */}
            <CardItem
              src = '../assets/dog_run.jpg'
              text='Organizations'
              label='Adventure'
              path = {findOrg}
              link = '/organizations'
            />
            {/* </Col>
          </Row> */}

            </ul>
            </div>
        </div>
    </div>
  );
}

export default homeCard