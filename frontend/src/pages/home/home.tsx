import React from 'react';
import { Button, Row, Col } from "react-bootstrap";
import HeroSection from './HeroSection';
import Card from './homeCard';
import './home.css';


function Home(){
    return(
        <>
        <HeroSection />
        <Card />
        <div id = 'bottom'></div>
        </>
    );
}
export default Home;

// Todo: create cards, create routeg