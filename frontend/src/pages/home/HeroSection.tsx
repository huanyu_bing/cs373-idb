import React from 'react'
import home_img from '../../assets/dog_run.jpg'
import './home.css';


function HeroSection() {
 return(
        <div>
            <div className="header">
                <div className="hero-text-box">
                    <h1 className="homeh1">FIND YOUR PETS</h1>
                    <a className="click btn-full" href="/pets">Find Pets</a>
                    <a className="click btn-ghost" href="#bottom">Show me more </a>
                </div>
            </div>
        </div>
    );
}

export default HeroSection