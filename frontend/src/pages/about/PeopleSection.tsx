import React, { useState, useEffect } from "react"
import axios from "axios"
// import Card from "react-bootstrap/Card"
// import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import {Container, Card, CardGroup, ListGroup} from 'react-bootstrap';

import '../../Components/Cards.css';
import Josephi_img from '../../assets/JosephLi.jpg';
import Kate_img from '../../assets/KateLee.jpg';
import Yihan_img from '../../assets/yihanXi.jpg';
import Koshik_img from '../../assets/kmahapatra.jpg';


// The following codes are inspired by Group Lowball and Sizhan Xu
// original codes: https://gitlab.com/lowball/cs373-idb/-/blob/3-add-gitlab-api/front-end/src/pages/About.tsx

const names = new Set(['Kevin-2017', "Kevin Wang", 'Yihan Xi', 'Joseph Li', 'Kate Lee', 'Koshik Mahapatra','Kate','yihanX','unknown','yihan xi'])

interface ICommitsByPerson {
  name: string
  email: string
  commits: number
  additions: number
  deletions: number
}

interface IIssuesByPerson {
  iid: string
  title: string
  author: string
  assignee: any
}

interface IPersonCard {
  name: string
  commits: number
  issues: number
  bio: string
  role: string
  avatarURL: any
}

const styles = {
  card: {
    borderRadius: 35,
    padding: '1rem',
    width: "18rem"
  },
  cardImage: {
    borderRadius: 35,
    height: '18rem',
    // width: '100%'
    // objectFit:'cover'
  }
}





const PeopleSection = () => {
    const[commits, setCommits] = useState<ICommitsByPerson[]>([]);
    const [issues, setIssues] = useState<IIssuesByPerson[]>([])


    // variable, functions that manipulate the count
    //Grab the data 
    useEffect(() => {
        axios.get('https://gitlab.com/api/v4/projects/39616531/repository/contributors')
        .then((res) => setCommits(res.data))
        axios.get('https://gitlab.com/api/v4/projects/39616531/issues?per_page=100')
        .then((res) => setIssues(res.data.map((issue:any) => {
            return {
                iid: issue.iid,
                title: issue.title, 
                author: issue.author.username,
                assignee: issue.assignee
            }
        }),
        ),
        )
        //
    },[])

    return(
      <div>
         <PersonGrid
          commits = {commits}
          issues = {issues}
         />
         <div>
          <h1>Repository Data</h1>
        <Card className="repoCard">
          <ListGroup variant="flush">
            <ListGroup.Item>Number of total commits:{" "}
          {commits.map((person) => person.commits).reduce((a, b) => a + b, 0)}</ListGroup.Item>
            <ListGroup.Item>Number of total issues: {issues.length}</ListGroup.Item>
          </ListGroup>
        </Card>
        </div>
      </div>
    )
    
}
    const PersonCard = ({ name, commits, issues, bio, role, avatarURL }: IPersonCard) => {
  return (
    <Card style={styles.card}  className = "cards__item__link card1" >
      {/* <div className = 'cards_img_con'> */}
      <Card.Img variant="top" src={avatarURL} style={styles.cardImage} className = "card_img1"/>
      {/* </div> */}
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{bio}</Card.Text>
        <Card.Text>{role}</Card.Text>
        <Card.Text>Number of commits: {commits}</Card.Text>
        <Card.Text>Number of issues: {issues}</Card.Text>
      </Card.Body>
    </Card>
  )
}

// Not Robust, need update later 
// Get the individual number of commits
const num_commits = (commits:ICommitsByPerson[], names:any) => {
  let commits_map = new Map<string,number>();
  for (let commit of commits){
    if (names.has(commit.name)){
      commits_map.set(commit.name, (Number(commits_map.get(commit.name)) + commit.commits) || commit.commits)
    }
  }
  return (commits_map)
}

const num_issues = (issues:IIssuesByPerson[]) => {
  let issues_map = new Map<string,number>();
  for (const issue of issues) {
    const author = issue.assignee;
    if(author != null){
      // console.log(issue.assignee.name)
        issues_map.set(issue.assignee.name, Number(issues_map.get(issue.assignee.name)) + 1 | 1)
    }
    
  }
  return issues_map;
};



const PersonGrid = (props: {commits:ICommitsByPerson[], issues:IIssuesByPerson[]}) => {
  const commits_map: Map<string, number> = num_commits(props.commits, names)
  const issues_map: Map<string, number> = num_issues(props.issues)
  console.log(commits_map)
  return (
    <Container className="d-grid gap-3">
      <Row xs={1} md={2} lg={3} className="d-flex justify-content-evenly">
        <Col>
          <PersonCard
            name="Yihan Xi"
            issues={Number(issues_map.get('yihan xi')) | 0}
            commits={Number(commits_map.get('yihanX'))|0 + Number(commits_map.get('yihan xi')) | 0}
            role = 'Full Stack Developer'
            bio = "I’m a junior CS and math double major from Houston, TX. In my free time, I like to write mysteries, try different foods and reading"
            avatarURL= {Yihan_img}
          />
        </Col>
        <Col>
          <PersonCard
            name="Kevin Wang"
            issues = {Number(issues_map.get('Kevin Wang')) | 0}
            commits = {(Number(commits_map.get('Kevin-2017'))|0) + (Number(commits_map.get('Kevin Wang')) | 0)}
            role = 'Full Stack Developer'
            bio = "I'm a junior at UT Austin. Currently I lived in a two-dimential world as a bird. In my free time I enjoy being a bird."
            avatarURL="https://avatars.githubusercontent.com/u/31030008?v=4"
          />
        </Col>
        <Col>
          <PersonCard
            name="Kate Lee"
            issues={Number(issues_map.get('KateLee')) | 0}
            commits={Number(commits_map.get('Kate')) | 0}
            role = 'Full Stack Developer'
            bio = "I'm a junior CS and math double major from Austin, TX. In my free time, I like to cook, try new food places, and play the piano."
            avatarURL= {Kate_img}
          />
        </Col>
        <Col>
          <PersonCard
            name="Koshik Mahapatra"
            issues={Number(issues_map.get('Koshik Mahapatra')) | 0}
            commits={Number(commits_map.get('Koshik Mahapatra')) | 0}
            role = 'Full Stack Developer'
            bio = "I'm a senior CS major from Stamford, CT. In my free time, I like to write and watch basketball."
            avatarURL= {Koshik_img}
          />
        </Col>
        <Col>
          <PersonCard
            name="Joseph Li"
            issues={Number(issues_map.get('Joseph Li')) | 0}
            commits={Number(commits_map.get('Joseph Li')) + Number(commits_map.get('unknown')) | 0}
            role = 'Full Stack Developer'
            bio = "I'm a second year computer science major at UT Austin studying under their Turing Scholars program. I'm from Frisco, TX and in my free time I enjoy playing Ultimate Frisbee, reading, and gaming. "
            avatarURL = {Josephi_img}
          />
        </Col>
      </Row>
    </Container>
  )
}


export default PeopleSection