import React, {useState, useEffect} from 'react';
import axios from "axios";
import {Container, Card, CardGroup, ListGroup} from 'react-bootstrap';

import PeopleSection from './PeopleSection';
import './about.css';

import petfinder_img from '../../assets/petfinder_img.png';
import maps_img from '../../assets/maps_img.png';
import yelp_img from '../../assets/yelp_img.png';

import gitlab_img from '../../assets/gitlab_img.png';
import postman_img from '../../assets/postman_img.png';
import react_img from '../../assets/react_img.png';
import docker_img from '../../assets/docker_img.png';
import aws_img from '../../assets/aws_img.png';





//  Codes inpisred by group 3 lowball
// interface IC


export default function About() {

  return (
    <Container>
      <div>
        <h1>About Us</h1>
        <p>
          Roughly 6.5 million animals enter shelters each year but only 3.2 million are adopted” (American Society for the Prevention of Cruelty to Animals). We want to make it easier for people to find and adopt pets and also connect them to organizations or clinics that can help care for them.
          <strong>Ready, Set, Pet! </strong>is a webpage that will link users to agencies with pets available for adoption. The webpage will also provide users with information regarding nearby veterinary hospitals in case their pets get sick.
          </p>
          <p>Our API Documentation can be found here: <a href="https://documenter.getpostman.com/view/23533144/2s83tGoC3z">https://documenter.getpostman.com/view/23533144/2s83tGoC3z</a>
          </p>
      </div>

      
      <div>
        <h1>Members</h1>
        <PeopleSection />
      </div>
      <h1>APIs</h1>
      <CardGroup>
        {APIS.map((a) =>
          <Card className="toolCard">
            <Card.Img variant="top" src={a.image} />
            <Card.Body>
              <Card.Title>{a.name}</Card.Title>
              <Card.Text>
                {a.description}
              </Card.Text>
              <Card.Link href={a.link}>Link</Card.Link>
            </Card.Body>
          </Card>
        )}
      </CardGroup>
      <h1>Tools</h1>
      <CardGroup>
        {TOOLS.map((t) =>
          <Card className="toolCard">
            <Card.Img variant="top" src={t.image} />
            <Card.Body>
              <Card.Title>{t.name}</Card.Title>
              <Card.Text>
                {t.description}
              </Card.Text>
              <Card.Link href={t.link}>Link</Card.Link>
            </Card.Body>
          </Card>
        )}
      </CardGroup>
    </Container>
  );
}


// var MEMBERS: Member[] = [
//   {
//     name: 'Koshik',
//     photo: 'koshik_img',
//     bio: '',
//     role: '',
//     commits: 0,
//     issues: 0,
//     tests: 0
//   }

// ]

interface Tool {
  name: string;
  description: string;
  link: string;
  image: any;
}

var TOOLS: Tool[] = [
  {
    name: 'Gitlab',
    description: 'Version control',
    link: 'https://gitlab.com/huanyu_bing/cs373-idb',
    image: gitlab_img
  },
  {
    name: 'Postman',
    description: 'API design',
    link: 'https://www.postman.com/',
    image: postman_img
  },
  {
    name: 'React',
    description: 'Front end development',
    link: 'https://reactjs.org/',
    image: react_img
  },
  {
    name: 'Docker',
    description: 'Front end development',
    link: 'https://www.docker.com/',
    image: docker_img
  },
  {
    name: 'AWS Amplify',
    description: 'Front end hostingt',
    link: 'https://aws.amazon.com/amplify/',
    image: aws_img
  }

]

var APIS: Tool[] = [
  {
    name: 'Petfinder',
    description: 'API for pet data and adoption orgs',
    link: 'https://www.petfinder.com/developers/v2/docs/#endpoints',
    image: petfinder_img
  },
  {
    name: 'Google Maps',
    description: 'API for location/review data of adoption orgs',
    link: 'https://developers.google.com/maps/documentation/places/web-service/supported_types',
    image: maps_img
  },
  {
    name: 'Yelp',
    description: 'API for vets',
    link: 'https://www.yelp.com/developers/documentation/v3/business',
    image: yelp_img
  }


]







