import React from "react";
// import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Route, useNavigate} from 'react-router-dom';
import { NullLiteral } from "typescript";
import CardPage from "../../Components/CardPage";
import '../../Components/Cards.css';
import Highlighter from "react-highlight-words";


// import {Container, Card, CardGroup, ListGroup} from 'react-bootstrap';

interface item {
  pos: string;
  id: string;
  rating: number;
  closed: boolean;
  review_count: number;
  location: any;
  phone: string;
  query?: string;
  name: string;
  photos: string;
}

const VetCard = (props : item) => {   
  let navigate = useNavigate();  
  var available;
  if(props.closed) {
    available = "Closed";
  } else available = "Open";
  // destructuring props
  return (
    <button className="col-md-3 card my-3 offset-md-1 border-1 py-2 cards__item__link card2" onClick={() => navigate("../vets/" + props.pos)}>
      <div className="card-img-top text-center">
        <img src={props.photos} alt={props.name} className="card_img1" />
      </div>
      <div className="card-body">
        <div className="card-title fw-bold fs-4">
          <Highlighter
            highlightClassName="highlighter"
            searchWords={props.query?.split(" ") ?? []}
            autoEscape={true}
            textToHighlight={String(props.name)} 
          />
        </div>
        <div>Availability: {available}</div>

        <div>
          Phone:
           <Highlighter
            highlightClassName="highlighter"
            searchWords={props.query?.split(" ") ?? []}
            autoEscape={true}
            textToHighlight={String(props.phone)} 
          />
           </div>
        <div>Rating: {props.rating}</div>
        <div>Total Reviews: {props.review_count}</div>
      </div>
    </button>
  );
};
 
export default VetCard;