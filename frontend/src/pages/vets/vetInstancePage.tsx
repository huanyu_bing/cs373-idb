import React, {useState} from 'react';
import {Link,useParams} from "react-router-dom";
import MapContainer from "../../Components/Map";

import CardItem from './CardItem.js';
import axios, { AxiosResponse } from "axios";
import Vet from "./vetsInterface";
import { API_URL } from "../../global";



function VetsInstancePage() {
    let id: string = useParams().VetId ?? "1";
    return <VetInstancePageCC id={id} />;
  }
  
  interface VetInstancePageProps {
    id: string;
  }
  
  interface VetInstancePageState {
    vet: Vet | null;
  }
  
  class VetInstancePageCC extends React.Component<
      VetInstancePageProps,
      VetInstancePageState
  > {
    constructor(props: any) {
      super(props);
      this.state = { vet: null};
    }
    componentDidMount() {
      this.loadVet();
    }
  
    async loadVet() {
      // console.log(this.props.id)
      let response: AxiosResponse<any, any> = await axios.get(
        `${API_URL}/vets/${this.props.id}`
      );
      
      let vet: Vet | null = response.data["data"]["vet"];
      this.setState({
          vet: vet,
      });
    }

    

    render() {
    console.log("before")
    if (this.state.vet == null) return;
    let vet: Vet = this.state.vet!;
    // console.log("sup")
    // console.log(vet)
    return (
        <div className="container-fluid">
            <div className="row">
            <h1 className="col-12 text-center my-3 fw-bold"> { vet.name }</h1>
            </div>
            <div className="row justify-content-center mt-5">
            <div className="col-4">
                <img src = {vet.image_url} className="photo w-75" />
            </div>
            <div className="col-4">
                <div className="card">
                    <div className="card-body">
                    <h5 className="card-title">About</h5>
                    <ul>
                        <li>Name: {vet.name}</li>
                        <li>Phone: {vet.display_phone}</li>
                        <li>City: {vet.city}</li>
                        <li>State: {vet.state}</li>
                        <li>Zipcode: {vet.zip_code}</li>
                        <li>Review Count : {vet.review_count}</li>
                        <li>Rating : {vet.rating}</li>
                        <li>Address: {vet.display_address}</li>
                        <li>Website: <a href="url">{vet.url}</a> </li>
                        <li>Availability: {vet.is_closed ? "Closed" : "Open"}</li>
                    </ul>
                </div>
            </div>
            </div>
        </div>
        <div className='cards'>
        <h1>Find us on Yelp!</h1>
        <div className='cards__container'>
            <div className='cards__wrapper'>
            <ul className='cards__items'>
                <CardItem
                src="yelp_img.png"
                text='Connect us now ^_^'
                label='Social Media'
                path={vet.url}
                />
            </ul>
            </div>
        </div>
        </div>
        <div className="row justify-content-center mt-5 text-center">
            <div className="col">
            <h5> Related Information </h5>
            </div>
        </div>
        <hr/>
        <div className="row justify-content-center mt-5">
            <div className="col-4">
            <div className="card">
                <div className="card-body">
                <h5 className="card-title">Nearest Pets</h5>
                    {/* <Link to="/petinstance0">Jade</Link> <br/>
                    <Link to="/petinstance1">Cassie</Link><br/>
                    <Link to="/petinstance2">North</Link> */}
                    {vet.closest_pets === ""  && <li>No pets found</li>}
                    {vet.closest_pets !== "" &&
                    <a href = {`../pets/${vet.closest_pets}`}>Direct me to related pets</a>}
                </div>
            </div>
            </div>
            <div className="col-4">
            <div className="card">
                <div className="card-body">
                <h5 className="card-title">Nearest Organization</h5>
                <ul>
                <a href = {`../orgs/${vet.closest_org}`}>Direct me to the nearest Organization</a>
                </ul>
                </div>
            </div>
            </div>
        </div>
        <div>
            <MapContainer lat={vet.lat} lng = {vet.lang}/>
        </div>
        </div>
    );
}
}

export default VetsInstancePage;
