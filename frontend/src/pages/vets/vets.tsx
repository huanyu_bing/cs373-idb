import React, {useState, useEffect} from 'react';
import './vets.css';
import Data from './VetData.json';
import VetCard from './VetCard';
import Container from 'react-bootstrap/Container';
import {Stack, Typography } from "@mui/material";
import Sidebar from "react-sidebar";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CardPage from "../../Components/CardPage";
import { useSearchParams } from "react-router-dom";
import axios, { AxiosResponse } from "axios";
import { API_URL } from "../../global";
import Vet from "./vetsInterface";
import{
  VetFilters, 
  VetSortOptions
}
from "./vetFilters"
import { ModelToolbar } from "../../toolbar/ModelToolbar";
import Spinner from 'react-bootstrap/Spinner';





export default function Vets(props: any) {
  let [searchParams, setSearchParams] = useSearchParams();
  let [vets, setVets] = useState<Vet[] | null>(null);
  let [total, setTotal] = useState<number>(0);
  let [pageCount, setPageCount] = useState<number>(0);
  let [page, setPage] = useState<number>(0);
  let [pageSize, setPageSize] = useState<number>(0);
  let [focusFields, setFocusFields] = useState<Set<string>>(new Set<string>()); 

  useEffect(() => {
    const fetchData = async () => {
      setVets(null);
      setTotal(0);
      let requiredFields: string = "thumbnailUrl,description";
      let newParams: URLSearchParams = new URLSearchParams(
        searchParams.toString()
      );
      newParams.set("required", requiredFields);
      console.log(newParams);
      let response: AxiosResponse<any, any> = await axios.get(
        `${API_URL}/vets?` + newParams
      );
      let data = response.data["data"];
      setVets(data["vet"]);
      setTotal(data["total"]);
      setPage(data["page"]);
      setPageSize(data["pageSize"]);
      setPageCount(Math.ceil(data["total"] / data["pageSize"]));
    };
    fetchData();
  }, [searchParams]);

  const updateQuery = (param: string, value: string) => {
    let newParams: URLSearchParams = new URLSearchParams(
      searchParams.toString()
    );
    newParams.set(param, value);
    setSearchParams(newParams);
  };

  return (
    <div>
      <Container fluid>
        <Row>
          <Col md = {11} className = "text-center">
            <h1 className= "title">Vets</h1>
            <Container>
            <ModelToolbar
              exactFilters={VetFilters}
              sortOptions={VetSortOptions}
              defaultSortOptionIndex={0}
              sortAscending={false}
              onFocusFieldsUpdate={setFocusFields}
            />
          </Container>
            <Container>
              {vets === null &&<Spinner animation="border"/>}
              <Col lg = {12}>
                <Row>
                  {vets !== null && (
                    <Stack direction="row" flexWrap="wrap">
                      {vets.map((val) => (<VetCard 
                        pos = {val.id}
                        id = {val.id}
                        name = {val.name}
                        location = {val.display_address}
                        photos = {val.image_url}
                        rating = {val.rating}
                        query = {searchParams.get("searchFilter") ?? undefined}
                        review_count = {val.review_count}
                        closed = {val.is_closed}
                        phone = {val.display_phone}
                      ></VetCard>))}
                    </Stack>
                    )}
                </Row>
              </Col>
            </Container>
          </Col>
        </Row>
        <CardPage
        page={page}
        pageSize={pageSize}
        pageCount={pageCount}
        total={total}
        onPageChange = {(page) => updateQuery("page", page.toString())}
        />
      </Container>
    </div>
  );
}
