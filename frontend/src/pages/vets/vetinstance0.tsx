import React, {useState} from 'react';
import {Link} from "react-router-dom";
import MapContainer from "../../Components/Map";
import CardItem from './CardItem.js';

const Vet0 = () => {
  var instances = require('./VetData.json');
  var data = instances[0]
  return (
    <div className="container-fluid">
        <div className="row">
          <h1 className="col-12 text-center my-3 fw-bold"> { data.name }</h1>
        </div>
        <div className="row justify-content-center mt-5">
          <div className="col-4">
            <img src = {data.image_url} className="photo w-75" />
          </div>
          <div className="col-4">
            <div className="card">
                <div className="card-body">
                  <h5 className="card-title">About</h5>
                  <ul>
                    <li>Phone: {data.display_phone}</li>
                    <li>Review Count : {data.review_count}</li>
                    <li>Rating : {data.rating}</li>
                    <li>Address: {data.location.display_address}</li>
                    <li>Availability: {data.is_closed ? "Closed" : "Open"}</li>
                  </ul>
              </div>
          </div>
        </div>
      </div>
      <div className='cards'>
      <h1>Find us on Yelp!</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
              src="yelp_img.png"
              text='Connect us now ^_^'
              label='Social Media'
              path={data.url}
            />
          </ul>
        </div>
      </div>
    </div>
      <div className="row justify-content-center mt-5 text-center">
        <div className="col">
          <h5> Related Information </h5>
        </div>
      </div>
      <hr/>
      <div className="row justify-content-center mt-5">
        <div className="col-4">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Nearest Pets</h5>
                <Link to="/petinstance0">Jade</Link> <br/>
                <Link to="/petinstance1">Cassie</Link><br/>
                <Link to="/petinstance2">North</Link>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Nearest Organization</h5>
              <ul>
                <Link to="/orginstance0"> Hill Country SPCA </Link>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div>
        <MapContainer lat={data.coordinates.latitude} lng = {data.coordinates.longitude}/>
      </div>
    </div>
  );
}

export default Vet0;
