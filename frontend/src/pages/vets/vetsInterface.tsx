interface Vet {
    city: string;
    closest_org: string;
    closest_pets: string | null;
    display_address: string;
    display_phone: string;
    id: string;
    image_url: string;
    is_closed: boolean;
    lang: number;
    lat: number;
    name: string;
    rating: number;
    review_count: number;
    state: string;
    url: string;
    zip_code: string;
  }
  
  export default Vet;
  