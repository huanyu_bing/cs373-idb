import { ExactFilter, SortOption } from "../../toolbar/ModelToolbar";
import cityOption from "./cityOption";
import zipOption from "./zipOption";


const VetFilters: ExactFilter[] = [
//  {
    // label: "Open Now",
    // field: "is_closed",
    // options: [
    //   {
    //     label: "Open",
    //     value: "false",
    //   },
    //   {
    //     label: "Close",
    //     value: "true",
    //   },
    // ],
  // },
  {
    label: "Zip Code",
    field: "zip_code",
    options : zipOption.map((f) => {
      return {label:f, value:f};
    })
  },
  {
    label: "State",
    field: "state",
    options : [
      {
       label:"TX",
       value:"TX"
      }
    ],
  },
  {
    label: "City",
    field: "city",
    options : cityOption.map((f) => {
      return {label:f, value:f};
    })
  }
]


const VetSortOptions: SortOption[] = [
  {
    label: "Name",
    field: "name"
  },
  {
    label: "Rating",
    field: "rating",
  },
  {
    label: "Review Count",
    field: "review_count",
  }
]


export { VetFilters, VetSortOptions};