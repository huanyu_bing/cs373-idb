import React from "react";
import { useParams } from "react-router-dom";
import { NavigateFunction, useNavigate } from "react-router-dom";
import axios, { AxiosResponse } from "axios";
import whitesquare from '../../assets/whiteSquare.png';
import Org from "./orgInterface";

import {useState} from 'react';
import qmark from '../../assets/qmark.png'
import {Link} from "react-router-dom";
import MapContainer from "../../Components/Map";
import { omitTriggerPropKeys } from "rsuite/esm/Picker";
import { checkPrimeSync } from "crypto";
import { API_URL } from "../../global";



function OrgInstancePage() {
  let id: string = useParams().OrgId ?? "1";
  return <OrgInstancePageCC id={id}/>;
}

interface OrgInstancePageProps {
  id: string;
}

interface OrgInstancePageState {
  org: Org | null;
}

class OrgInstancePageCC extends React.Component<
    OrgInstancePageProps,
    OrgInstancePageState
> {
  constructor(props: any) {
    super(props);
    this.state = {org: null};
  }

  componentDidMount() {
    this.loadOrganization();
  }

  async loadOrganization() {
    // console.log(this.props.id)
    let response: AxiosResponse<any, any> = await axios.get(
      `${API_URL}/orgs/${this.props.id}`
    );
    
    let org: Org | null = response.data["data"]["organization"];
    this.setState({
        org: org,
    });
  }

  render() {
    if (this.state.org == null) return;
    let org: Org = this.state.org!;
    console.log(org)
    return (
        <div className="container-fluid">
        <div className="row">
          <h1 className="col-12 text-center my-3 fw-bold"> { org.name }</h1>
        </div>
        <div className="row justify-content-center mt-5">
          <div className="col-4">
            <img src = {(org.photos === "None") ? whitesquare : org.photos} className="photo w-75"/>
          </div>
          <div className="col-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">About</h5>
                <ul>
                  <li>Name: {org.name}</li>
                  <li>Rating: {org.rating}</li>
                  <li>Availability: {org.open_now ? "Open" : "Closed"}</li>
                  <li>Email: {org.email}</li>
                  <li>Phone: {org.phone}</li>
                  <li>Website: <a href = "url">{org.website}</a></li>
                  <li>Address : {org.formatted_address} </li>
                  <li>Rating Count : {org.rating_count} </li>
                  <li>Business Status : {org.business_status} </li>
                  <li>Hours : {org.hours} </li>
                </ul>
              </div>
            </div>
          </div>
      </div>
      <div className="row justify-content-center mt-5 text-center">
        <div className="col">
          <h5> Related Information </h5>
        </div>
      </div>
      <hr/>
      <div className="row justify-content-center mt-5">
        <div className="col-4">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Nearest Pets</h5>
                {org.pets === ""  && <li>No pets found</li>}
                {org.pets !== "" &&
                <Link to="/petinstance0">Jade</Link>}
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Closest Vets/Animal Hospitals</h5>
              <ul>
                <a href = {`../vets/${org.closest_vet}`}>Direct me to the nearest Vet</a>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div>
        <MapContainer lat={org.lat} lng = {org.lang}/>
      </div>
    </div>
    );
  }
}

export default OrgInstancePage;
