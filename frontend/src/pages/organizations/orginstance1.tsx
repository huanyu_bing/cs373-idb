import React, {useState} from 'react';
import qmark from '../../assets/qmark.png'
import {Link} from "react-router-dom";
import MapContainer from "../../Components/Map";

const Org1 = () => {
  var instancesPetFinder = require('./OrganizationDataPetFinder.json');
  var instancesMaps = require('./OrganizationDataGoogle.json');
  
  var petFinderData = instancesPetFinder[1]
  var googleMapsData = instancesMaps[1]
  if(petFinderData.photos.length === 0) {
    petFinderData.photos[0] = {"large" : qmark};
  }
  return (
    <div className="container-fluid">
        <div className="row">
          <h1 className="col-12 text-center my-3 fw-bold"> { petFinderData.name }</h1>
        </div>
        <div className="row justify-content-center mt-5">
          <div className="col-4">
            <img src = {qmark} className="photo w-75"/>
          </div>
          <div className="col-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">About</h5>
                <ul>
                  <li>Rating: {googleMapsData.rating}</li>
                  <li>Availability: {googleMapsData.opening_hours.open_now ? "Open" : "Closed"}</li>
                  <li>Email: {petFinderData.email}</li>
                  <li>Phone: {petFinderData.phone}</li>
                  <li>Website: <a href = "url">{petFinderData.website}</a></li>
                  <li>Location: 
                    <ul>
                      <li>Address : {petFinderData.address.address1}</li>
                      <li>City : {petFinderData.address.city}</li>
                      <li>State : {petFinderData.address.state}</li>
                      <li>Zipcode : {petFinderData.address.postcode}</li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
      </div>
      <div className="row justify-content-center mt-5 text-center">
        <div className="col">
          <h5> Related Information </h5>
        </div>
      </div>
      <hr/>
      <div className="row justify-content-center mt-5">
        <div className="col-4">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Nearest Pets</h5>
                <Link to="/petinstance0">Jade</Link> <br/>
                <Link to="/petinstance1">Cassie</Link><br/>
                <Link to="/petinstance2">North</Link>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">3 Closest Vets/Animal Hospitals</h5>
              <ul>
                <Link to="/vetinstance0"> 1. Cat Doctor </Link> <br />
                <Link to="/vetinstance1"> 2. Midtown Veterinary Hospital </Link><br />
                <Link to="/vetinstance2"> 3. PetCare Express </Link>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div>
        <MapContainer lat={googleMapsData.geometry.location.lat} lng = {googleMapsData.geometry.location.lng}/>
      </div>
    </div>
  );
}

export default Org1;
