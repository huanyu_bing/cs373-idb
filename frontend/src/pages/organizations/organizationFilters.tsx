import { ExactFilter, SortOption } from "../../toolbar/ModelToolbar";


const OrganizationFilters: ExactFilter[] = [
 {
    label: "Open Now",
    field: "open_now",
    options: [
      {
        label: "Open",
        value: "true",
      },
      {
        label: "Close",
        value: "flase",
      },
    ],

  },
  {
    label: "business status",
    field: "business_status",
    options: [
      {
        label: "Operational",
        value: "OPERATIONAL",
      },
      {
        label: "Temporarily Closed",
        value: "CLOSED_TEMPORARILY",
      },
       {
        label: "Permanently Closed",
        value: "CLOSED_PERMANENTLY",
      },
      
    ],
  }

]


const OrganizationSortOptions: SortOption[] = [
  {
    label: "Name",
    field: "name",
  },
  {
    label: "Rating",
    field: "rating",
  },
  {
    label: "Rating Count",
    field: "rating_count"
  }
]



export { OrganizationFilters, OrganizationSortOptions};