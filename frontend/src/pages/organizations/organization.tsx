import React, {useState, useEffect } from 'react';
import './organization.css';
// import whitesquare from '../../assets/whiteSquare.png';
// import Data from './OrganizationDataPetFinder.json';

import OrgCard from './OrganizationCard';
import Container from 'react-bootstrap/Container';
import {Stack, Typography } from "@mui/material";
import Sidebar from "react-sidebar";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CardPage from "../../Components/CardPage";
import { useSearchParams } from "react-router-dom";
import axios, { AxiosResponse } from "axios";
import { API_URL } from "../../global";
import{
  OrganizationFilters, 
  OrganizationSortOptions
}
from "./organizationFilters"

import {ModelToolbar} from "../../toolbar/ModelToolbar";
import Spinner from 'react-bootstrap/Spinner';


import Org from "./orgInterface";

export default function Organization(props: any) {
  let [searchParams, setSearchParams] = useSearchParams();
  let [orgs, setOrgs] = useState<Org[] | null>(null);
  let [total, setTotal] = useState<number>(0);
  let [pageCount, setPageCount] = useState<number>(0);
  let [page, setPage] = useState<number>(0);
  let [pageSize, setPageSize] = useState<number>(0);
  let [focusFields, setFocusFields] = useState<Set<string>>(new Set<string>()); 
  //need to update focusFields



  useEffect(() => {
    const fetchData = async () => {
      setOrgs(null);
      setTotal(0);
      let requiredFields: string = "thumbnailUrl,description";
      let newParams: URLSearchParams = new URLSearchParams(
        searchParams.toString()
      );
      newParams.set("required", requiredFields);
      console.log(newParams);
      let response: AxiosResponse<any, any> = await axios.get(
        `${API_URL}/orgs?` + newParams
      );
      let data = response.data["data"];
      setOrgs(data["organization"]);
      setTotal(data["total"]);
      setPage(data["page"]);
      setPageSize(data["pageSize"]);
      setPageCount(Math.ceil(data["total"] / data["pageSize"]));
    };
    fetchData();
  }, [searchParams]);

  const updateQuery = (param: string, value: string) => {
    let newParams: URLSearchParams = new URLSearchParams(
      searchParams.toString()
    );
    newParams.set(param, value);
    setSearchParams(newParams);
  };


  // const [items, setItem] = useState(Data);
  return (
    <div>
      <Container fluid>
       
        <Row>
          {/* <Col md = {1} style={{borderRight: "2px solid black", height: "100vh"}}>
            <Row >
              Filters:
            </Row>
          </Col> */}
          
          <Col md = {11} className = "text-center">
            <p className = 'title'>Organizations</p>
          <Container>
            <ModelToolbar
              exactFilters={OrganizationFilters}
              sortOptions={OrganizationSortOptions}
              defaultSortOptionIndex={0}
              sortAscending={false}
              onFocusFieldsUpdate={setFocusFields}
            />
          </Container>
            <Container>
              {orgs === null &&<Spinner animation="border"/>}
              <Col lg = {12}>
                <Row>
                  {orgs !== null && (
                    <Stack direction="row" flexWrap="wrap">
                      {orgs.map((val) => (<OrgCard
                        pos = {val.id}
                        id = {val.id}
                        name = {val.name}
                        email = {val.email}
                        address = {val.formatted_address}
                        query = {searchParams.get("searchFilter") ?? undefined}
                        media = {val.instagram_url}
                        phone = {val.phone}
                        photos = {val.photos}
                        statement = {val.open_now}
                      ></OrgCard>))}
                    </Stack>
                    )}
                </Row>
              </Col>
            </Container>
          </Col>
        </Row>
        <CardPage
        page={page}
        pageSize={pageSize}
        pageCount={pageCount}
        total={total}
        onPageChange = {(page) => updateQuery("page", page.toString())}
      />
      </Container>
    </div>
  );
}
