interface Org {
    business_status: string;
    closest_vet: string;
    email: string;
    facebook_url: string | null;
    formatted_address: string;
    hours: string | null;
    id: string;
    instagram_url: string;
    lang: number;
    lat: number;
    name: string;
    open_now: boolean;
    pets: string | null;
    phone: string;
    photos: string;
    rating: number ;
    rating_count: number;
    twitter_url: string | null;
    website: string | null;
  }
  
  export default Org;
  