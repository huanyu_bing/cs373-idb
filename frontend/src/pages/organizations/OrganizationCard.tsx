import React from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Route, useNavigate} from 'react-router-dom';
import { StepItemProps } from "rsuite";
import whitesquare from '../../assets/whiteSquare.png';
import Highlighter from "react-highlight-words";
import qmark from '../../assets/qmark.png';


interface item {
  pos: string;
  id: string;
  name: string;
  email: any;
  phone: string;
  media: any;
  query?: string;
  photos: string;
  address: any;
  statement: any;
}
// function mission(statement : any) {
//   if(statement == null) {
//     return "N/A";
//   } else return statement;
// }
const OrgCard = (props : item) => {   
  let navigate = useNavigate();  
  // destructuring props
  return (

        // <button className="col-md-2 card my-3 py-3 border-1" onClick={() => navigate("../vets" + props.pos)}>

    <button className="col-md-3 card my-3 py-3 border-1" onClick={() => navigate(`../orgs/${props.id}`)}>
      <div className="card-img-top text-center">
        <img src={(props.photos === "None") ? qmark : props.photos} alt={props.name} className="photo w-75" />
      </div>
      <div className="card-body">
        <div className="card-title fw-bold fs-4 ">
          <Highlighter
            highlightClassName="highlighter"
            searchWords={props.query?.split(" ") ?? []}
            autoEscape={true}
            textToHighlight={String(props.name)} 
          />
        </div>
        <div className="text-break">Email:    
        <i>        
        <Highlighter
            highlightClassName="highlighter"
            searchWords={props.query?.split(" ") ?? []}
            autoEscape={true}
            textToHighlight={String( props.email)} 
          />
        </i>
        </div>
        <div className="card-email">Phone: <Highlighter
            highlightClassName="highlighter"
            searchWords={props.query?.split(" ") ?? []}
            autoEscape={true}
            textToHighlight={String(props.phone)} 
          />
        </div>
        <div className = "address">Address: <Highlighter
            highlightClassName="highlighter"
            searchWords={props.query?.split(" ") ?? []}
            autoEscape={true}
            textToHighlight={String(props.address)} 
          /></div>
        {/* <div>Mission Statement: {mission(props.statement)}</div> */}
      </div>
    </button>
  );
};

export default OrgCard;