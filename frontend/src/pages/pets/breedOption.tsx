const breedOption = ['Plott Hound', 'Australian Cattle Dog / Blue Heeler', 'Cattle Dog', 'Husky', 'Terrier', 'Pit Bull Terrier', 'Chihuahua', 'Domestic Short Hair', 'Labrador Retriever', 'Catahoula Leopard Dog', 'Pekingese', 'Mixed Breed', 'Domestic Long Hair', 'Border Collie', 'Carolina Dog', 'Shepherd', 'Domestic Medium Hair', 'Hound', 'Retriever']

export default breedOption