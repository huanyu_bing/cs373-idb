const attributes = {
   Age:['Adult','Baby','Senior','Young'],
   Type:['Dog','Cat','Rabbit'],
   Other:['age','gender','size']
}

export default attributes