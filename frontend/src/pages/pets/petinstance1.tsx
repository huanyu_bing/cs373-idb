import React, {useState} from 'react';
import './petinstance.css';
import {Link} from "react-router-dom";
import MapContainer from "../../Components/Map";

const PetsInstance = () => {
  var instances = require('./PetData.json');
  var data = instances[1]
  return (
    <div className="container-fluid">
        <div className="row">
          <h1 className="col-12 text-center my-3 fw-bold"> { data.name }</h1>
        </div>
        <div className="row justify-content-center mt-5">
          <div className="col-4">
            <img src = {data.photos[0].large} className="photo w-75"/>
          </div>
          <div className="col-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">About</h5>
                <ul>
                  <li>Type: {data.type}</li>
                  <li>Gender: {data.gender}</li>
                  <li>Size: {data.size}</li>
                  <li>Age: {data.age}</li>
                  <li>Status: {data.status}</li>
                  <li>Breeds: {data.breeds.primary}</li>
                  <li>Colors: {data.colors.primary}</li>
                  <li>Organization: Killeen Animal Services</li>
                  <li>Contact Information: 
                    <ul>
                      <li>Phone : {data.contact.phone}</li>
                      <li>Address : {data.contact.address.address1}</li>
                      <li>City : {data.contact.address.city}</li>
                      <li>State : {data.contact.address.state}</li>
                      <li>Zipcode : {data.contact.address.postcode}</li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
      </div>
      <div className="row justify-content-center mt-5 text-center">
        <div className="col">
          <h5> Related Information </h5>
        </div>
      </div>
      <hr/>
      <div className="row justify-content-center mt-5">
        <div className="col-4">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Organization</h5>
                <Link to="/orginstance1">Killeen Animal Services</Link>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">5 Closest Vets/Animal Hospitals</h5>
              <ul>
                <Link to="/vetinstance0"> 1. Cat Doctor </Link> <br />
                <Link to="/vetinstance1"> 2. Midtown Veterinary Hospital </Link><br />
                <Link to="/vetinstance2"> 3. PetCare Express </Link>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div>
        <MapContainer lat={data.geometry.location.lat} lng = {data.geometry.location.lng}/>
      </div>
    </div>
  );
}

export default PetsInstance;
