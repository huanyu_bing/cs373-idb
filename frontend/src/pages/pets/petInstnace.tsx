import React, {useState} from 'react';
import './petinstance.css';
import {Link,useParams} from "react-router-dom";
import MapContainer from "../../Components/Map";
import axios, { AxiosResponse } from "axios";
import Pet from "./petsInterface";
import { API_URL } from "../../global";
import whitesquare from '../../assets/whiteSquare.png';


function PetsInstance() {
    let id: string = useParams().PetId ?? "1";
    return <PetInstancePageCC id={id} />;
  }
  
  interface PetInstancePageProps {
    id: string;
  }
  
  interface PetInstancePageState {
    pet: Pet | null;
  }
  
  class PetInstancePageCC extends React.Component<
    PetInstancePageProps,
    PetInstancePageState
  > {
    constructor(props: any) {
      super(props);
      this.state = { pet: {} as Pet};
    }
    componentDidMount() {
      this.loadPet();
    }
  
    async loadPet() {
      // console.log(this.props.id)
      let response: AxiosResponse<any, any> = await axios.get(
        `${API_URL}/pets/${this.props.id}`
      );
      
      let pet: Pet | null = response.data["data"]["pet"];
      this.setState({
        pet: pet,
      });
    }
  
    render() {

      if (this.state.pet == null) return;
      let pet: Pet = this.state.pet!;
    return (
        <div className="container-fluid">
            <div className="row">
            <h1 className="col-12 text-center my-3 fw-bold"> { pet.name }</h1>
            </div>
            <div className="row justify-content-center mt-5">
            <div className="col-4">
                <img src = {(pet.photo_url === "None") ? whitesquare : pet.photo_url} className="photo w-75"/>
            </div>
            <div className="col-4">
                <div className="card">
                <div className="card-body">
                    <h5 className="card-title">About</h5>
                    <ul>
                    <li>Name: {pet.name}</li>
                    <li>Type: {pet.type}</li>
                    <li>Gender: {pet.gender}</li>
                    <li>Size: {pet.size}</li>
                    <li>Age: {pet.age}</li>
                    <li>Status: {pet.status}</li>
                    <li>Breeds: {pet.breeds}</li>
                    {/* <li>Colors: {pet.colors.primary}</li> */}
                    <li>Organization: Hill Country SPCA </li> 
                    <li>Contact Information: 
                        <ul>
                        <li>Phone : {pet.phone}</li>
                        <li>Address : {pet.address}</li>
                        {/* <li>City : {pet.contact.address.city}</li>
                        <li>State : {pet.contact.address.state}</li>
                        <li>Zipcode : {pet.contact.address.postcode}</li> */}
                        </ul>
                    </li>
                    </ul>
                </div>
                </div>
            </div>
        </div>
        <div className="row justify-content-center mt-5 text-center">
            <div className="col">
            <h5> Related Information </h5>
            </div>
        </div>
        <hr/>
        <div className="row justify-content-center mt-5">
            <div className="col-4">
            <div className="card">
                <div className="card-body">
                <h5 className="card-title">Organization</h5>
                <a href = {`../orgs/${pet.organization_id}`}>Direct me to the Organization</a>
                </div>
            </div>
            </div>
            <div className="col-4">
            <div className="card">
                <div className="card-body">
                <h5 className="card-title">Closest Vets/Animal Hospitals</h5>
                <ul>
                  {pet.closest_vet === ""  && <li>No pets found</li>}
                  {pet.closest_vet !== "" &&
                  <a href = {`../vets/${pet.closest_vet}`}>Direct me to closest vets</a>}
                </ul>
                </div>
            </div>
            </div>
        </div>
        <div>
            <MapContainer lat={pet.lat} lng = {pet.lang}/>
        </div>
        </div>
    );
    }
}

export default PetsInstance;
