import React, {useState, useEffect} from 'react';
import './pets.css';
import Data from './PetData.json';
import PetCard from './PetCard';
import Container from 'react-bootstrap/Container';
import {Stack, Typography } from "@mui/material";
import Sidebar from "react-sidebar";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CardPage from "../../Components/CardPage";
import { useSearchParams } from "react-router-dom";
import axios, { AxiosResponse } from "axios";
import { API_URL } from "../../global";
import {PetFilters, PetSortOptions} from "./petFilters"
import { ModelToolbar } from "../../toolbar/ModelToolbar";
import Spinner from 'react-bootstrap/Spinner';
import whitesquare from '../../assets/whiteSquare.png';
import catNone from '../../assets/cat_none2.png'



import Pet from "./petsInterface";

export default function Pets(props: any) {
  let [searchParams, setSearchParams] = useSearchParams();
  let [pets, setPets] = useState<Pet[] | null>(null);
  let [total, setTotal] = useState<number>(0);
  let [page, setPage] = useState<number>(0);
  let [pageCount, setPageCount] = useState<number>(0);
  let [pageSize, setPageSize] = useState<number>(0);
  let [focusFields, setFocusFields] = useState<Set<string>>(new Set<string>()); 

  useEffect(() => {
    const fetchData = async () => {
      setPets(null);
      setTotal(0);
      let requiredFields: string = "thumbnailUrl,description";
      let newParams: URLSearchParams = new URLSearchParams(
        searchParams.toString()
      );
      newParams.set("required", requiredFields);
      console.log(newParams);
      let response: AxiosResponse<any, any> = await axios.get(
        `${API_URL}/pets?` + newParams
      );
      let data = response.data["data"];
      setPets(data["pet"]);
      setTotal(data["total"]);
      setPage(data["page"]);
      setPageSize(data["pageSize"]);
      setPageCount(Math.ceil(data["total"] / data["pageSize"]));
    };
    fetchData();
  }, [searchParams]);

  const updateQuery = (param: string, value: string) => {
    let newParams: URLSearchParams = new URLSearchParams(
      searchParams.toString()
    );
    newParams.set(param, value);
    setSearchParams(newParams);
  };

  // if(val.photo_url === "None") {
  //   if(props.type == "Cat") {
  //     props.photos = catNone;
  //   }
  //   else{
  //     props.photos = whitesquare;
  //   }
  // }
  return (
    <div>
      <Container fluid>
        <Row>
          <Col md = {11} className = "text-center">
            <h1 className= "title">Pets</h1>
            <Container>
            <ModelToolbar
              exactFilters={PetFilters}
              sortOptions={PetSortOptions}
              defaultSortOptionIndex={0}
              sortAscending={false}
              onFocusFieldsUpdate={setFocusFields}
            />
          </Container>
            <Container>
              {pets === null &&<Spinner animation="border"/>}
              <Col lg = {12}>
                <Row>
                  {pets !== null && (
                    <Stack direction="row" flexWrap="wrap">
                      {pets.map((val) => (<PetCard 
                        pos = {val.id}
                        id = {val.id}
                        organization = {val.organization_id}
                        type = {val.type}
                        query = {searchParams.get("searchFilter") ?? undefined}
                        // colors = {val.colors}
                        species = {val.type}
                        age = {val.age}
                        gender = {val.gender}
                        size = {val.size}
                        name = {val.name}
                        photos = {val.photo_url} //(val.photo_url == "None") ? whitesquare :
                      ></PetCard>))}
                    </Stack>
                    )}
                </Row>
              </Col>
            </Container>
          </Col>
        </Row>
        <CardPage
        page={page}
        pageSize={pageSize}
        pageCount={pageCount}
        total={total}
        onPageChange = {(page) => updateQuery("page", page.toString())}
        />
      </Container>
    </div>
  );
}
