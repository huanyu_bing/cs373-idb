import React from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Route, useNavigate} from 'react-router-dom';
import whitesquare from '../../assets/whiteSquare.png';
import catNone from '../../assets/cat_none2.png'
import dogNone from '../../assets/dog_none.png'
import rabbitNone from '../../assets/rabbit_none.jpg'
import Highlighter from "react-highlight-words";



interface item {
  pos: string;
  id: string;
  name: string;
  organization: string;
  type: string;
  species: string;
  query?: string;
  age: string;
  size: string;
  gender: string;
  photos: any;
}

const PetCard = (props : item) => {   
  let navigate = useNavigate();  
  console.log(props)
  let photos = props.photos
  if(photos == "None") {
    switch(props.type){
      case "Cat":
        photos = catNone;
        break;
      case "Dog":
        photos = dogNone;
        break;
      case "Rabbit":
        photos = rabbitNone;
        break;
      default:
        photos = whitesquare;
    }
  }
  
  // destructuring props
  return (
    
    <button className="col-md-3 card my-3 py-3 border-1" onClick={() => navigate("../pets/" + props.pos)}>
      {/* {props.query} */}
      <div className="card-img-top text-center">
        <img src={photos} alt={props.name} className="photo w-75" />
      </div>
      <div className="card-body">
        <div className="card-title fw-bold fs-4">
          <Highlighter
            highlightClassName="highlighter"
            searchWords={props.query?.split(" ") ?? []}
            autoEscape={true}
            textToHighlight={String(props.name)} 
          />
        </div>
        <div>
          Gender: {props.gender}
          </div>
        <div>Type: {props.type}</div>
        <div>Age: {props.age}</div>
        <div>Size: {props.size}</div>
      </div>
    </button>
  );
};
 
export default PetCard;