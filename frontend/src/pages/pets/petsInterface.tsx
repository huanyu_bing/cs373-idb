interface Pet {
    address: string;
    age: string;
    breeds: string;
    closest_vet: string;
    email: string;
    gender: string;
    id: string;
    lang: number;
    lat: number;
    name: string;
    organization_id: string;
    phone: string;
    photo_url: string;
    size: string;
    status: string;
    type: string;
  }
  
  export default Pet;
  