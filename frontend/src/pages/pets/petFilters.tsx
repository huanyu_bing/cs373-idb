import { ExactFilter, SortOption } from "../../toolbar/ModelToolbar";
import breedOption from "./breedOption";

const PetFilters: ExactFilter[] = [
 {
    label: "Type",
    field: "type",
    options: [
      {
        label: "Dog",
        value: "Dog",
      },
      {
        label: "Cat",
        value: "Cat",
      },
      {
        label: "Rabbit",
        value: "Rabbit",
      },
    ],

  },
  {
    label: "Breeds",
    field: "breeds",
    options : breedOption.map((f) => {
      return {label:f, value:f};
    })
  },
  {
    label: "Adopt status",
    field: "status",
    options: [
      {
        label: "Adoptable",
        value: "adoptable",
      },
      {
        label: "Adopted",
        value: "adopted",
      },
    ],
  },
  {
    label: "Gender",
    field: "gender",
    options: [
      {
        label: "Male",
        value: "Male",
      },
      {
        label: "Female",
        value: "Female",
      },
    ],
  }
]


const PetSortOptions: SortOption[] = [
  {
    label: "Name",
    field: "name"
  },
  {
    label: "Age",
    field: "age",
  },
  {
    label: "Size",
    field: "size",
  }
]


export { PetFilters, PetSortOptions};