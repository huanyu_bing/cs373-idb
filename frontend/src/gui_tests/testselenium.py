from selenium.webdriver.remote.webelement import WebElement
import pytest
from selenium import webdriver
from selenium.webdriver import Remote
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support import expected_conditions as EC
import time

driver = None
wait = None
local = False
url = "https://development.d3o6choxvs0bee.amplifyapp.com/"
PATH = "./chromedriver.exe"

def setup_module(module):
    print("starting setup_module")
    global driver, wait

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    if local:
        driver = webdriver.Chrome("./chromedriver.exe", options=chrome_options)
    else:
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
        
    
    wait = WebDriverWait(driver, 20)
    return driver

def test_home_page():
    print("Home page test")
    driver.get(url)
    assert driver.current_url == url

def test_home_page_css():
    print("Home page css test")
    driver.get(url)
    home_css = driver.find_elements(By.CSS_SELECTOR, value=".header")

def test_about_link():
    print("about test")
    driver.get(url)
    about_link = driver.find_element(By.LINK_TEXT, "About")
    about_link.click()
    driver.refresh()
    assert driver.current_url == url + "About"

def test_about_css():
    print("about css test")
    driver.get(url + "About")
    about_css = driver.find_elements(By.CLASS_NAME, 'toolCard')

# def test_pet_link():
#     print("pet test")
#     driver.get(url)
#     pet_link = driver.find_element(By.LINK_TEXT, "Pets")
#     pet_link.click()
#     driver.refresh()
#     assert driver.current_url == url + "Pets"

def test_pet_css():
    print("pet css test")
    driver.get(url + "Pets")
    pet_css = driver.find_elements(By.CSS_SELECTOR, value=".title")

def test_vet_link():
    print("vet test")
    driver.get(url)
    vet_link = driver.find_element(By.LINK_TEXT, "Vets")
    vet_link.click()
    driver.refresh()
    assert driver.current_url == url + "Vets"

def test_vet_css():
    print("vet css test")
    driver.get(url + "Vets")
    vet_css = driver.find_elements(By.CSS_SELECTOR, value=".title")

def test_org_link():
    print("org test")
    driver.get(url)
    org_link = driver.find_element(By.LINK_TEXT, "Organizations")
    org_link.click()
    driver.refresh()
    assert driver.current_url == url + "Organizations"

def test_org_css():
    print("org css test")
    driver.get(url + "Organizations")
    org_css = driver.find_elements(By.CSS_SELECTOR, value=".title")
    
def test_petsort():
    print("pet sort test")
    driver.get(url + "Pets?sortFilter=name")
    pet_name = driver.find_elements(By.CLASS_NAME, value="Anda")

def test_petsearch():
    print("pet sort test")
    driver.get(url + "Pets?searchFilter=Freddie+Purrcury")
    pet_name = driver.find_elements(By.CLASS_NAME, value="Freddie Purrcury")

def test_petfilter():
    print("pet filter test")
    driver.get(url + "Pets?breedsFilter=Chihuahua")
    pet_name = driver.find_elements(By.CLASS_NAME, value="Cornflakes")

def test_vetsort():
    print("vet sort test")
    driver.get(url + "Vets?sortFilter=name")
    pet_name = driver.find_elements(By.CLASS_NAME, value="Beechnut")

def test_vetsearch():
    print("vet sort test")
    driver.get(url + "Vets?searchFilter=Longenbaugh")
    pet_name = driver.find_elements(By.CLASS_NAME, value="Longenbaugh")

def test_vetfilter():
    print("vet filter test")
    driver.get(url + "Vets?cityFilter=Houston")
    pet_name = driver.find_elements(By.CLASS_NAME, value="Longenbaugh")

def test_orgsort():
    print("org sort test")
    driver.get(url + "Organizations?sortFilter=name")
    pet_name = driver.find_elements(By.CLASS_NAME, value="Comanche")

def test_orgsearch():
    print("org sort test")
    driver.get(url + "Organizations?searchFilter=Alamo")
    pet_name = driver.find_elements(By.CLASS_NAME, value="Alamo")

def test_orgfilter():
    print("org filter test")
    driver.get(url + "Organizations?business_statusFilter=CLOSED_PERMANENTLY")
    pet_name = driver.find_elements(By.CLASS_NAME, value="Purrfect")