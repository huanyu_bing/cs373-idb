import React from 'react';
import { GoogleMap, LoadScript, InfoWindow, Marker } from '@react-google-maps/api';
import { autocompleteClasses } from '@mui/material';

interface place{
    lat: number;
    lng: number;
}

const MapContainer = (coords : place) => {
  
  const mapStyles = {        
    height: "80vh",
    width: "80%",
    padding: "10px",
    top: "10vh",
    left: "10%",
    border: "10vh",
    }
    
  
  const defaultCenter = {
    lat: coords.lat, lng: coords.lng
  }
  
  return (
     <LoadScript
       googleMapsApiKey='AIzaSyCgKnk4LKCr48jHdKCEiCZj5zJbbC5Jwbk'>
        <GoogleMap
          mapContainerStyle={mapStyles}
          zoom={13}
          center={defaultCenter}
        >
            <Marker position={defaultCenter} />
        </GoogleMap>
     </LoadScript>
  )
}

export default MapContainer;