import {
    Button,
    Pagination,
    PaginationItem,
    Stack,
    Typography,
  } from "@mui/material";
import React from 'react';
import { useSearchParams } from "react-router-dom";
  
  interface CardPaginationProps {
    page: number;
    pageSize: number;
    pageCount: number;
    total: number;
    onPageChange: (page: number) => void;
  }
  
  function CardPagination(props: CardPaginationProps) {
    let [searchParams, setSearchParams] = useSearchParams();
    
    const handlePageChange = (newPage: number) => {
      let newParams = searchParams;
      newParams.set("page", newPage.toString());
      setSearchParams(newParams);
    };
  

    return (
      <Stack
        direction="column"
        justifyContent="center"
        alignContent="center"
        alignItems="center"
        gap="8px"
        margin="8px"
      >
        <Pagination
          sx={{ margin: "8px" }}
          count={props.pageCount}
          page = {props.page}
          color="primary"
          size="large"
          renderItem={(item) => (
            <PaginationItem
              component={Button}
              onClickCapture={() => handlePageChange(item.page ?? 1)}
              {...item}
            />
          )}
  
        />
        <Typography sx={{ textAlign: "center" }} variant="subtitle1" gutterBottom>
          Total Results: {props.total}
        </Typography>
      </Stack>
    );
  }
  
  export default CardPagination;
  
