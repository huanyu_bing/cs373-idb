import React from 'react';
import { Link } from 'react-router-dom';


// The following codes are build on top of https: //github.com/briancodex/react-website-v1/blob/master/src/components/CardItem.tsx*/

interface Card_info {
  src: string;
  text: string;
  label: string;
  path: string;
  link:string;
}

const CardItem = (props: Card_info) =>{
  return (
    <>
      <li className='cards__item'>
        <Link className='cards__item__link' to={props.link}>
          <figure className='cards__item__pic-wrap' data-category={props.label}>
            <img
              className='cards__item__img'
              alt='Pet image'
              src={props.path}
            />
          </figure>
          <div className='cards__item__info'>
            <h5 className='cards__item__text'>{props.text}</h5>
          </div>
        </Link>
      </li>
    </>
  );
}


export default CardItem