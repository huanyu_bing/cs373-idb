
// The below code are modify based on  https://gitlab.com/coleweinman/swe-college-project/-/tree/main/frontend/src/components/toolbar
import {Box, MenuItem, TextField,Tooltip,IconButton} from "@mui/material";
import {Button, Card, Stack } from 'react-bootstrap';
// import Button from 'react-bootstrap/Button';
// import Card from 'react-bootstrap/Card';
import {
  Clear,
  ArrowUpward as AscendingIcon,
  ArrowDownward as DescendingIcon,
} from "@mui/icons-material";

import React, { useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { ChipFilterField, Option } from "./ChipFilterField";

interface ModelToolbarProps {
  exactFilters: ExactFilter[];
  sortOptions: SortOption[];
  defaultSortOptionIndex?: number;
  sortAscending: boolean;
  onFocusFieldsUpdate?: (focusFields: Set<string>) => void;
}

interface ExactFilter {
  label: string;
  field: string;
  options?: Option[];
}

interface SortOption {
  label: string;
  field: string;
}

function ModelToolbar(props: ModelToolbarProps) {
  const [searchParams, setSearchParams] = useSearchParams();
  const [searchQuery, setSearchQuery] = React.useState<string>("");
  const [showFilters, setShowFilters] = React.useState<boolean>(false);
  const [sortOption, setSortOption] = React.useState<SortOption>(
    props.sortOptions[props.defaultSortOptionIndex ?? 0]
  );
  const [sortAscending, setSortAscending] = React.useState<boolean>(
    props.sortAscending
  );

  const handleSortChange = (option: SortOption) => {
    let newParams = searchParams;
    newParams.set("sortFilter", option.field);
    newParams.delete("page");
    setSearchParams(newParams);
  };

  const handleSortDirectionChange = (sortAscending: boolean) => {
    let newParams = searchParams;
    newParams.set("ascending", sortAscending ? "true" : "false");
    newParams.delete("page");
    setSearchParams(newParams);
  };

  const submitSearch = (query: string) => {
    let newParams = searchParams;
    if (query.length === 0) {
      newParams.delete("searchFilter");
    } else {
      newParams.set("searchFilter", query);
    }
    newParams.delete("page");
    setSearchParams(newParams);
  };

  const optionToValue = (option: SortOption): string => {
    return option.field;
  };

  const valueToOption = (value: string): SortOption => {
    let option: SortOption =
      props.sortOptions.find((o) => o.field === value) ??
      props.sortOptions[props.defaultSortOptionIndex ?? 0];
    return option;
  };

  const clearFilters = () => {
    let newParams = searchParams;
    for (let exactFilter of props.exactFilters) {
      newParams.delete(exactFilter.field + "Filter");
    }
    newParams.delete("searchFilter");
    newParams.delete("sortFilter");
    newParams.delete("ascending");
    newParams.delete("page");
    setSearchParams(newParams);
  };

  const getChipFilterFieldValue = (field: string) => {
    let param: string = searchParams.get(field + "Filter") ?? "";
    let paramValues: string[] = param === "" ? [] : param.split(",");
    return paramValues;
  };

  let onFocusFieldsUpdate = props.onFocusFieldsUpdate;
  let exactFilters = props.exactFilters;
  let sortOptions = props.sortOptions;
  let defaultSortOptionIndex = props.defaultSortOptionIndex;
  console.log(exactFilters)

  useEffect(() => {
    setSearchQuery(searchParams.get("searchFilter") ?? "");
    let sortAscending: boolean =
      searchParams.get("ascending") === "true" ? true : false;
    setSortAscending(sortAscending);
    let searchParamsString = searchParams.toString();
    if (
      // searchParamsString.includes("Max") ||
      // searchParamsString.includes("Min") ||
      searchParamsString.includes("Filter")
    ) {
      setShowFilters(true);
    }

    let sortFilter: string | null = searchParams.get("sortFilter");
    if (sortFilter !== null) {
      setSortOption(
        sortOptions.find((o) => o.field === sortFilter) ??
          sortOptions[defaultSortOptionIndex ?? 0]
      );
    } else {
      setSortOption(sortOptions[defaultSortOptionIndex ?? 0]);
    }
  }, [searchParams, sortOptions, defaultSortOptionIndex]);

  useEffect(() => {
    let newFocusFields = new Set<string>();
    for (let exactFilter of exactFilters) {
      if (searchParams.has(exactFilter.field + "Filter")) {
        newFocusFields.add(exactFilter.field);
      }
    }
    if (searchParams.has("sortFilter")) {
      newFocusFields.add(searchParams.get("sortFilter")!);
    }
    if (onFocusFieldsUpdate !== undefined) onFocusFieldsUpdate(newFocusFields);
  }, [
    searchParams,
    exactFilters,
    sortOptions,
    onFocusFieldsUpdate,
  ]);

  return (
    <Card style={{ width: 'auto' }}>
      <Card.Body>
        <Stack direction="vertical" gap={2}>
          <Stack direction="horizontal" gap={1}>
            <TextField
              id="search-field"
              label="Search"
              value={searchQuery}
              onKeyDown={(event) => {
                if (event.key === "Enter") {
                  submitSearch(searchQuery);
                }
              }}
              onChange={(event) => submitSearch(event.target.value)}
              sx={{
                borderRadius: "8px",
                flexGrow: 1,
                display: "flex",
              }}
              variant="outlined"
            />
            {props.sortOptions.length > 0 && (
              <TextField
                id="filter-field"
                select
                label="Sort"
                value={optionToValue(sortOption)}
                onChange={(event) =>
                  handleSortChange(valueToOption(event.target.value))
                }
                InputProps={{
                  sx: {
                    borderRadius: "8px",
                    flexGrow: 1,
                    minWidth: "150px",
                    display: "flex",
                  },
                }}
              >
                {props.sortOptions.map((option) => (
                  <MenuItem
                    key={optionToValue(option)}
                    value={optionToValue(option)}
                  >
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            )}
            {props.sortOptions.length > 0 && (
              <Box sx={{ display: "flex", alignItems: "center" }}>
                <Tooltip title={sortAscending ? "Ascending" : "Descending"}>
                  <IconButton
                    onClick={() => handleSortDirectionChange(!sortAscending)}
                  >
                    {sortAscending && <AscendingIcon />}
                    {!sortAscending && <DescendingIcon />}
                  </IconButton>
                </Tooltip>
              </Box>
            )}
          </Stack>
          {(
            <Stack direction="horizontal" gap={2} >
              {props.exactFilters.map((f) => (
                <Box sx={{ flexGrow: 1 }}>
                  <ChipFilterField
                    value={getChipFilterFieldValue(f.field)}
                    key={f.field}
                    label={f.label}
                    field={f.field}
                    options={f.options}
                    onChange={(value: string[]) => {
                      let newParams = searchParams;
                      if (value.length === 0) {
                        newParams.delete(f.field + "Filter");
                      } else {
                        newParams.set(f.field + "Filter", value.join(","));
                      }
                      newParams.delete("page");
                      setSearchParams(newParams);
                    }}
                  />
                </Box>
              ))}
            </Stack>
          )}
          {showFilters && (
            <Box sx={{ justifyContent: "center", display: "flex" }}>
              <Button onClick={() => clearFilters()}>Clear Filters</Button>
            </Box>
          )}
          </Stack>
      </Card.Body>
    </Card>

  );
}

export { ModelToolbar, ExactFilter, SortOption };
