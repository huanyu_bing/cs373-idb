import App from "./App";
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { BrowserRouter, MemoryRouter, useLocation } from "react-router-dom";
/*
test 1 - 2
*/
it('Home Page', () => {
   render(<BrowserRouter><App/></BrowserRouter>);

   expect(screen.getByText(/FIND YOUR PETS/)).toBeInTheDocument(); 
 
   expect(screen.getByText("Home")).toHaveAttribute('href', "/"); 

}); 
/*
test 3 - 8
*/
it('About Page', () => {
    render(<MemoryRouter initialEntries={['/about']}><App/></MemoryRouter>);
 
    expect(screen.getByText(/About Us/)).toBeInTheDocument(); 

    expect(screen.getByText(/Yihan Xi/)).toBeInTheDocument(); 
    expect(screen.getByText(/Kevin Wang/)).toBeInTheDocument();
    expect(screen.getByText(/Kate Lee/)).toBeInTheDocument(); 
    expect(screen.getByText(/Koshik Mahapatra/)).toBeInTheDocument();
    expect(screen.getByText(/Joseph Li/)).toBeInTheDocument(); 
 }); 
/*
test 9
*/
 it('Pets Page', () => {
    render(<MemoryRouter initialEntries={['/pets']}><App/></MemoryRouter>);
 
    expect(screen.getAllByRole("button")[0]).toBeInTheDocument(); 
 }); 
/*
test 10
*/
 it('Vets Page', () => {
    render(<MemoryRouter initialEntries={['/vets']}><App/></MemoryRouter>);
 
    expect(screen.getAllByRole("button")[0]).toBeInTheDocument(); 
 }); 
/*
test 11
*/
 it('Organizations Page', () => {
    render(<MemoryRouter initialEntries={['/organizations']}><App/></MemoryRouter>);
 
    expect(screen.getAllByRole("button")[0]).toBeInTheDocument(); 
 }); 

 