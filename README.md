<b> Canvas/Discord Group Number: </b> 10-11 <br>
<b> Names of the team members: </b> Kate Lee, Joseph Li, Koshik Mahapatra, Yihan Xi, Kevin Wang <br>
<b> Name of the project: </b> ReadySetPet <br>
<b> URL of the GitLab repo </b>  https://gitlab.com/huanyu_bing/cs373-idb <br>
<b> Link to website:  </b> [Development Version](https://development.d3o6choxvs0bee.amplifyapp.com/) <br>
<b>Main Page: </b> readysetpet.me <br>
<b>API Documentation: https://documenter.getpostman.com/view/23533144/2s83tGoC3z</b> 
<b> The proposed project: </b> <br>
Ready, Set, Pet! is a webpage that will link users to agencies with pets available for adoption. The webpage will also provide users with information regarding nearby veterinary hospitals in case their pets get sick. <br>

<b>Project Leader</b><br>
Phase 1 Lead: Kevin Wang, Koshik Mahaptra <br>
Phase 2 Lead: Joseph Li<br>
Phase 3 Lead: Kate Lee<br>
Phase 4 Lead: Yihan Xi <br>

<b>Git SHA: 70221bd840c7495adf41462fa06f01939df4d566</b><br>
<b>GitLab Pipelines: https://gitlab.com/huanyu_bing/cs373-idb/-/pipelines</b><br>
<b>Website: https://www.readysetpet.me/</b><br>
<b>Presentation: https://docs.google.com/presentation/d/1Nvj7kTbTJExy6So8doT3jbHglHeiLi5dZmQTFRMdctg/edit?usp=sharing</b><br>

<b>Completion Time (Phase I)</b>
| Time\Member     | Estimated | Actual |
|-----------------|-----------|--------|
| Kevin Wang      | 10        | 20     |
| Koshik Mahapatra | 10        | 15     |
| Joseph Li       | 10        | 15     |
| Kate Lee        | 10        | 15     |
| Yihan Xi       | 20        | 15      |

<b>Completion Time (Phase II)</b>
| Time\Member     | Estimated | Actual |
|-----------------|-----------|--------|
| Kevin Wang      | 20        | 30     |
| Koshik Mahapatra | 10        | 15     |
| Joseph Li       | 20        | 30     |
| Kate Lee        | 20        | 30     |
| Yihan Xi       | 20        | 15      |

<b>Completion Time (Phase III)</b>
| Time\Member     | Estimated | Actual |
|-----------------|-----------|--------|
| Kevin Wang      | 30        | 25     |
| Joseph Li       | 25        | 20     |
| Kate Lee        | 20        | 20     |
| Yihan Xi       | 20        | 20      |

<b>Completion Time (Phase IV)</b>
| Time\Member     | Estimated | Actual |
|-----------------|-----------|--------|
| Kevin Wang      | 10        | 12     |
| Joseph Li       | 10        | 10     |
| Kate Lee        | 5        | 5     |
| Yihan Xi       | 5        | 5      |

<b>  URLs of at least three disparate data sources: </b> <br>

[Google Map](https://developers.google.com/maps/documentation/places/web-service/supported_types) <br>
[Petfinder API](https://www.petfinder.com/developers/v2/docs/#endpoints) <br>
[Yelp API](https://www.yelp.com/developers/documentation/v3/business) <br>

<b> Three models and number of instances: </b> <br>
Pet (1000+), Organizations/Adoption Agency  (50+), Vet/Pet Hospital


<b> Describe at least five of those attributes for each model that you could filter by or sort by on the model (grid) pages:</b> <br>
| Model \Attributes | Pet                 | Vet/Pet Hospital | Organizations/Adoption Agency:               |
|-------------------|---------------------|------------------|----------------------------------------------|
| 1                 | Age                 | Distance (zip)   | Location from nearest to furthest - distance |
| 2                 | gender              | City             | City                                         |
| 3                 | size                | Open hours       | Animal type                                  |
| 4                 | Location (distance) | Rating           | Hours                                        |
| 5                 | Status              | Review Count     | Postcode                                     |
| 6                 |                     | Price            | rating                                       |
  

<b> Describe at least five additional attributes for each model that you could search for on the instance pages:
</b>




| Model \Attributes | Pet          | Vet/Pet Hospital    | Organizations/Adoption Agency: |
|-------------------|--------------|---------------------|--------------------------------|
| 1                 | Name         | Phone number        | Name                           |
| 2                 | color        | Email               | Email                          |
| 3                 | Animal Type  | Website url         | Phone                          |
| 4                 | Breed        | Status: open/closed | Website url                    |
| 5                 | Organization | Reviews             | address                        |


<b>Describe at least two types of media for each model that you could display on the instance pages:</b> <br>
Pet: Photos, videos, text/description
Vet/Pet Hospital: Photos, text, map, link to hospital
Organizations/Adoption Agency: photo, text, map, link to soacial media


<b> Data model connections: </b> <br>
Pet <-> Vet/Pet Hospital: Need to take pet to hospital after adopting the pet <br>
Pet <-> Organizations/Adoption Agency: Place to find pet to adopted <br>
Vet/Pet Hospital <-> Organizations/Adoption Agency: Connect by distance  <br>


<b>Three questions we answer:</b> <br>
What pets are available for adoption? <br>
Where can I find a place to take care of my pet? <br>
Where can I find places to adopt? <br>

<br>
Comments: <br>
The codes is cs373-idb/frontend/src/pagesabout/PeopleSection.tsx is inspired by Group Lowball and Sizhan Xu <br>
The card compoments in cs373-idb/frontend/src/Components/CardItem.tsx and Cards.css is based on a tutorial https://github.com/briancodex/react-website-v1/ <br>
The codes in cs373-idb/frontend/src/pagesabout/organization.tsx is inspired by Group UnivarCity. (so are the other 2 model pages)<br>
The codes in cs373-idb/frontend/src/pagesabout/organizationInstance.tsx is inspired by Group UnivarCity. (so are the other 2 instance pages)<br>
The Frontend Filterble, sortable and searable implimentation is inspired by Group UnivarCity. https://gitlab.com/coleweinman/swe-college-project/-/tree/main/frontend/src/components/toolbar<br>


